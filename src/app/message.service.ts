import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  private messages: any = {}

  constructor() {
    
  }

  sendMessage(message: any, to: string) {
    this.messages[to] = message
  }

  clearMessages(to: string) {
    this.messages[to] = ''
  }

  getMessage(to) {
    return this.messages[to]
  }
}
