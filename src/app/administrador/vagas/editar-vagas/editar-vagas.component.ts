import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CargosService } from 'src/app/modelos/cargos/cargos.service';
import { CampusService } from 'src/app/modelos/campus/campus.service';
import { MessageService } from 'src/app/message.service';
import { VagasService } from 'src/app/modelos/vagas/vagas.service';

@Component({
  selector: 'app-editar-vagas',
  templateUrl: './editar-vagas.component.html',
  styleUrls: ['./editar-vagas.component.scss']
})
export class EditarVagasComponent implements OnInit {
  id_vaga: number
  area: any
  alerta_de_vagas = true
  alerta_erro: boolean = false
  mensagem: any = false
  mensagem_error: any = false
 
  dataSource: any
  dataSourceCampus: any
  dataSourceCargo: any
  vagasForm: FormGroup;

  constructor(private router: Router, private formBuilder: FormBuilder, private _apiCargo: CargosService, private route: ActivatedRoute,
    private _apiCampus: CampusService, private messageService: MessageService, private _apiVaga: VagasService) { }

    async ngOnInit() {
    this.route.params.subscribe(params => {
      this.id_vaga = +params['id']; // (+) converts string 'id' to a number
    })

    this.criarFormulario()

    try {
      this.dataSource = await this.getVaga()
      this.area = await this.getArea(this.dataSource.cargo)
      this.dataSourceCampus = await this.getCampus()
      this.dataSourceCargo = await this.getCargo()
      this.setDadosVaga(this.dataSource)   
    }

    catch (err) {
      console.error(err)
    }
     
  }

  getVaga() {
    return new Promise((resolve, reject) => {
      this._apiVaga.getVaga(this.id_vaga)
        .subscribe((res: any) => {         
          resolve(res.data[0])
        }, err => {
          reject(err)
        })
    })
  }

  getArea(cargoAtual) {
    return new Promise((resolve, reject) => {
      this._apiCargo.getCargoID(cargoAtual)
        .subscribe((res: any) => {
          resolve(res.data[0].area)
        }, err => {
          reject(err)
        })
    })
  }

  getCampus() {
    return new Promise((resolve, reject) => {
      this._apiCampus.getCampus()
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  getCargo() {
    return new Promise((resolve, reject) => {
      this._apiCargo.getCargo()
        .subscribe((res: any) => {
          if(res.data.length > 0) {
            this.alerta_de_vagas = false
          }
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  criarFormulario() {
    this.vagasForm = this.formBuilder.group({
      'campus': [null, Validators.required],
      'area': [null, Validators.required],
      'cargo': [null, Validators.required]
    })    
  }

  onChangeArea(deviceValue) {
    this._apiCargo.getCargoArea(deviceValue)
    .subscribe((res:any) => {
      this.dataSourceCargo = res.data
    }, err => {
      console.log(err)
    })
  }

  setDadosVaga(dados) {
    this.vagasForm.setValue({
      campus: dados.campus,      
      area: this.area,
      cargo: dados.cargo,
    })
  }


  postVagas(data) {
    let form = {
      campus: data.campus,
      cargo: data.cargo
    }

    this._apiVaga.patchVaga(form, this.id_vaga)
      .subscribe(res => {
          this.alerta_erro = false
          this.messageService.sendMessage('Vaga editada com Sucesso!', 'vagas')
          this.router.navigate(['/Admin/Vagas']);
        }, (err) => {
          console.log(err);
          this.alerta_erro = true
        });
  }


}
