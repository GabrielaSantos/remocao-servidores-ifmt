import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from 'src/app/message.service';
import { VagasService } from 'src/app/modelos/vagas/vagas.service';
import { CargosService } from 'src/app/modelos/cargos/cargos.service';
import { CampusService } from 'src/app/modelos/campus/campus.service';
import { ListasService } from 'src/app/modelos/listas/listas.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as moment from 'moment';
import { DadosService } from 'src/app/modelos/inscricoes/dados.service';
import { ReprovacoesService } from 'src/app/modelos/reprovacoes/reprovacoes.service';
import { BloqueiosService } from 'src/app/modelos/bloqueios/bloqueios.service';
import { RemocaoService } from 'src/app/modelos/inscricoes/remocao.service';
import { AprovacoesService } from 'src/app/modelos/aprovacoes/aprovacoes.service';


@Component({
  selector: 'app-visualizar-vagas',
  templateUrl: './visualizar-vagas.component.html',
  styleUrls: ['./visualizar-vagas.component.scss']
})
export class VisualizarVagasComponent implements OnInit {
  alerta_de_inscrito
  mensagem: any = false
  mensagem_error: any = false
  id_vaga: any
  usuarioForm: FormGroup
  aprovacaoForm: FormGroup
  alerta_servidor
  
  dadosVagas: any
  dataSource: any
  dataSourceUsuario: any
  dataSourceCampus:any
  dataSourceCargo: any
  dataSourceArea: any
  dataSourceLista:any
  dataSourceAprovacao:any
  dataSourceInscricao:any
  usuarioAprovado: any
  status: any
  botao_publicar: any
  campus: any
  cargo: string
  area: string

  constructor(private router: Router, private messageService: MessageService, private _apiVaga: VagasService,
     private route: ActivatedRoute, private _apiUsuario: DadosService, private _apiCargo: CargosService, 
     private _apiCampus: CampusService, private _apiLista: ListasService, private formBuilder: FormBuilder,
     private _apiReprovacao: ReprovacoesService, private _apiInscricao: RemocaoService, 
     private _apiBloqueios: BloqueiosService, private _apiAprovacao: AprovacoesService) { }

  async ngOnInit() {
    this.criarFormulario()
    this.mensagem = this.messageService.getMessage('admin-atribuir-success')
    this.messageService.clearMessages('admin-atribuir-success')

    try {
      this.route.params.subscribe(params => {
        this.id_vaga = +params['id'];
      })

      this.dataSource = await this.getVaga()

      if(this.dataSource[0].status == 'criado') {
        this.dataSourceLista = await this.getLista(this.dataSource[0].campus, this.dataSource[0].cargo)
        this.usuarioAprovado = this.dataSourceLista[0]
        
        this.dataSourceUsuario = (!this.alerta_de_inscrito ? await this.getUsuario(this.usuarioAprovado.id_user) : '')
        this.alerta_servidor = true
      }
      else {
        this.dataSourceAprovacao = await this.getAprovacao(this.dataSource[0].id)
        this.dataSourceInscricao = await this.getInscricao(this.dataSourceAprovacao[0].inscricao)

        this.dataSourceUsuario = await this.getUsuario(this.dataSourceInscricao[0].usuario)
        this.status = (this.dataSourceAprovacao[0].aceite == 0 ? 'Aguardando Servidor!' : 'Remoção Aceita!')
        this.botao_publicar = (this.dataSourceAprovacao[0].aceite == 1 ? true : false)
        this.alerta_servidor = false
      }
      
     
      if(this.dataSourceUsuario.length > 0) {
        this.dataSourceCampus = await this.getCampus()
        this.campus = await this.getCampusVaga(this.dataSource[0].campus)
  
        this.dataSourceArea = await this.getArea(this.dataSourceUsuario[0].cargo)
        this.area = (this.dataSourceArea == 1 ? 'Professor do Ensino Básico, Técnico e Tecnológico' : 'Técnico')
  
        this.dataSourceCargo = await this.getCargo(this.dataSourceUsuario[0].cargo)
        this.cargo = this.dataSourceCargo[0].nome
  
        this.setDadosPessoais(this.dataSourceUsuario[0])
      }
     
    }
    catch (err) {
      console.error(err)
    }
  }

  getVaga() {
    return new Promise((resolve, reject) => {
      this._apiVaga.getVaga(this.id_vaga)
        .subscribe((res: any) => {         
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  getAprovacao(vaga) {
    return new Promise((resolve, reject) => {
      this._apiAprovacao.getAprovacao(vaga)
        .subscribe((res: any) => {         
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  getInscricao(id) {
    return new Promise((resolve, reject) => {
      this._apiInscricao.getInscricaoID(id)
        .subscribe((res: any) => { 
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }
  
  patchVagas(vaga) {
    return new Promise((resolve, reject) => {
      this._apiVaga.patchVaga(vaga, this.id_vaga)
        .subscribe((res: any) => {         
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  getUsuario(id) {
    return new Promise((resolve, reject) => {
      this._apiUsuario.getUsuarioID(id)
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  getCampusVaga(id) {
    return new Promise((resolve, reject) => {
      this._apiCampus.getCampusID(id)
        .subscribe((res: any) => {
          resolve(res.data[0].nome)
        }, err => {
          reject(err)
        })
    })
  }

  getCampus() {
    return new Promise((resolve, reject) => {
      this._apiCampus.getCampus()
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  getArea(cargoAtual) {
    return new Promise((resolve, reject) => {
      this._apiCargo.getCargoID(cargoAtual)
        .subscribe((res: any) => {
          resolve(res.data[0].area)
        }, err => {
          reject(err)
        })
    })
  }

  getCargo(cargo) {
    return new Promise((resolve, reject) => {
      this._apiCargo.getCargoID(cargo)
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }
  
  getLista(campus, cargo) {
    return new Promise((resolve, reject) => {
      this._apiLista.getLista(campus, cargo)
        .subscribe((res: any) => {
          this.alerta_de_inscrito = (res.length > 0 ? false : true)    
          resolve(res)
        }, err => {
          reject(err)
        })
    })
  }

  criarFormulario() {
    this.usuarioForm = this.formBuilder.group({
      'nome': [null, Validators.required],
      'matricula': [null, Validators.required],
      'email': [null, Validators.required],
      'telefone': [null, Validators.required],
      'nascimento': [null, Validators.required],
      'campus_atual': [null, Validators.required],
      'data_ingresso_campus': [null, Validators.required],
      'data_ingresso_ifmt': [null, Validators.required],
      'area': [null, Validators.required],
      'cargo': [null, Validators.required]
    })
    this.aprovacaoForm = this.formBuilder.group({
      'observacoes_avaliacao': [null, Validators.required],
      'avaliar': [null]
    })
  }

  setDadosPessoais(dados) {
    this.usuarioForm.setValue({
      nome: dados.nome,
      matricula: dados.matricula,
      email: dados.email,
      telefone: dados.telefone,
      nascimento: {
        day: Number.parseInt(moment(dados.nascimento).utc().format('DD')),
        month: Number.parseInt(moment(dados.nascimento).utc().format('MM')),
        year: Number.parseInt(moment(dados.nascimento).utc().format('YYYY'))
      },
      campus_atual: dados.campus_atual,
      data_ingresso_campus: {
        day: Number.parseInt(moment(dados.data_ingresso_campus).utc().format('DD')),
        month: Number.parseInt(moment(dados.data_ingresso_campus).utc().format('MM')),
        year: Number.parseInt(moment(dados.data_ingresso_campus).utc().format('YYYY'))
      },
      data_ingresso_ifmt: {
        day: Number.parseInt(moment(dados.data_ingresso_ifmt).utc().format('DD')),
        month: Number.parseInt(moment(dados.data_ingresso_ifmt).utc().format('MM')),
        year: Number.parseInt(moment(dados.data_ingresso_ifmt).utc().format('YYYY'))
      },
      area: this.area,
      cargo: dados.cargo
    })
  }

  async enviarDados(form: any) {
    let newForm: any
    try {
      if(form.avaliar == 0) {
        newForm = {
          justificativa: form.observacoes_avaliacao,
          inscricao: this.usuarioAprovado.id_inscricao,
          vaga: this.id_vaga
        }
        let formInscricao = {
          ativar: 0
        }

        await this.postReprovar(newForm)
        await this.setInscricao(formInscricao, this.usuarioAprovado.id_inscricao)
        await this.setBloqueio(newForm.justificativa, this.usuarioAprovado.id_user)
        this.messageService.sendMessage("Servidor(a) "+ this.usuarioAprovado.nome +" Bloqueado(a)!", 'vagas')
        this.router.navigate(['/Admin/Vagas'])
      }
      else {
        let vaga = {
          status: 'atribuido'
        }
        let formInscricao = {
          aprovacao: 1
        }

        let aprovacao = {
          inscricao: this.usuarioAprovado.id_inscricao,
          vaga: this.id_vaga,
          campus_origem: this.dataSourceUsuario[0].campus_atual,
          campus_destino: this.dataSource[0].campus,
        }
        await this.patchVagas(vaga)
        await this.setInscricao(formInscricao, this.usuarioAprovado.id_inscricao)
        await this.postAprovar(aprovacao)

        this.ngOnInit()
      }
    }

    catch (err) {
      console.error(err)
    }
  }

  postAprovar(form) {
    return new Promise((resolve, reject) => {
      this._apiAprovacao.postAprovacao(form)
        .subscribe((res: any) => {
          this.messageService.sendMessage("Vaga Atribuída com Sucesso!", 'admin-atribuir-success') 
          resolve(res)
        }, err => {
          this.mensagem_error = "Erro ao Aprovar Servidor!"
          reject(err)
        })
    })
  }

  patchAprovar(form, id) {
    return new Promise((resolve, reject) => {
      this._apiAprovacao.patchAprovacao(form, id)
        .subscribe((res: any) => {
          resolve(res)
        }, err => {
          this.mensagem_error = "Erro ao Publicar Remoção!"
          reject(err)
        })
    })
  }

  postReprovar(form) {
    return new Promise((resolve, reject) => {
      this._apiReprovacao.postReprovacao(form)
        .subscribe((res: any) => {
          resolve(res)
        }, err => {
          this.mensagem_error = "Erro ao Reprovar Servidor!"
          reject(err)
        })
    })
  }

  setInscricao(form, id) {
    return new Promise((resolve, reject) => {    
      this._apiInscricao.patchInscricao(form, id)
        .subscribe((res: any) => {   
          resolve(res)
        }, err => {
          reject(err)
        })
    })
  }

  setBloqueio(mensagem, id) {
    let bloqueio = {
      usuario: id,
      justificativa: mensagem
    }

    return new Promise((resolve, reject) => {
      this._apiBloqueios.postBloqueios(bloqueio)
        .subscribe((res: any) => {   
          resolve(res)
        }, err => {
          reject(err)
        })
    })
  }

  async excluirAtribuicao(form) {
    let newForm = {
      justificativa: form.observacoes_avaliacao,
      inscricao: this.dataSourceInscricao[0].id,
      vaga: this.id_vaga
    }

    let formInscricao = {
      ativar: 0
    }

    let vaga = {
        status: 'criado'
    }

    await this.postReprovar(newForm)
    await this.setInscricao(formInscricao, this.dataSourceInscricao[0].id)
    await this.setBloqueio(newForm.justificativa, this.dataSourceUsuario[0].id)
    await this.patchVagas(vaga)

    this.messageService.sendMessage("Servidor "+ this.dataSourceUsuario[0].nome +" Bloqueado!", 'vagas')
    this.router.navigate(['/Admin/Vagas'])
  }

  async publicarRemocao() {
    let newForm = {
      publicar: 1
    }
    let formInscricao = {
      aprovacao: 3
    }
    let vaga = {
      ativar: 0,
      status: 'publicado'
    }
    await this.patchAprovar(newForm, this.dataSourceAprovacao[0].id)
    await this.setInscricao(formInscricao, this.dataSourceAprovacao[0].inscricao)
    await this.patchVagas(vaga)

    this.messageService.sendMessage("Remoção Finalizada e Publicada com Sucesso!", 'vagas') 
    this.router.navigate(['/Admin/Vagas'])
  }

}
