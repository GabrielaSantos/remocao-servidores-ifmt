import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizarVagasComponent } from './visualizar-vagas.component';

describe('VisualizarVagasComponent', () => {
  let component: VisualizarVagasComponent;
  let fixture: ComponentFixture<VisualizarVagasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizarVagasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizarVagasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
