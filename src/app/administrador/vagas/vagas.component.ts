import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { MessageService } from 'src/app/message.service';
import { VagasService } from 'src/app/modelos/vagas/vagas.service';
import { CargosService } from 'src/app/modelos/cargos/cargos.service';
import { CampusService } from 'src/app/modelos/campus/campus.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vagas',
  templateUrl: './vagas.component.html',
  styleUrls: ['./vagas.component.scss']
})
export class VagasComponent implements OnInit {
  alerta_de_vagas = true
  mensagem: any = false
  id_vaga: any

  dadosVagas: any
  dataSource: any
  dataSourceCampus:any
  dataSourceCargo: any
  dataSourceArea: any
  array: any

  itemsPerPage = 20
  currentPage: number = 1;

  constructor(private router: Router, private messageService: MessageService, private _apiVaga: VagasService,
    private _apiCargo: CargosService, private _apiCampus: CampusService) { }

  async ngOnInit() {
    this.mensagem = this.messageService.getMessage('vagas')
    this.messageService.clearMessages('vagas')

    try {
      this.dataSource = await this.getVaga()      
      this.dataSourceCampus = await this.getCampus()
      this.dataSourceCargo = await this.getCargo()
      this.dadosVagas = await this.setDados()      
    } 

    catch (err) {
      console.error(err)
    }
  }
  
  getVaga() {
    return new Promise((resolve, reject) => {
      this._apiVaga.getVagas()
        .subscribe((res: any) => {
          if(res.data.length > 0) {
            this.alerta_de_vagas = false
          }
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  getCampus() {
    return new Promise((resolve, reject) => {
      this._apiCampus.getCampus()
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  getCargo() {
    return new Promise((resolve, reject) => {
      this._apiCargo.getCargo()
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  setDados() {
    this.dataSource.forEach((element, i) => {
     this.dataSource[i].campus = this.setCampus(element.campus)
     this.dataSource[i].area = this.setArea(element.cargo)
     this.dataSource[i].cargo = this.setCargo(element.cargo)
    })
    return this.dataSource
  }

  setCampus(campus) {
    let array = {}
    array =  this.dataSourceCampus.filter((item) => {
      return (item.id == campus); 
    })
    return array[0].nome
  }

  setCargo(cargo) {
    let array = {}
    array =  this.dataSourceCargo.filter((item) => {
      return (item.id == cargo); 
    })
    return array[0].nome
  }

  setArea(cargo) {
    let array:any
    let area
    array =  this.dataSourceCargo.filter((item) => {
      return (item.id == cargo); 
    })
    area = (array[0].area == 1 ? 'Professor do Ensino Básico, Técnico e Tecnológico' : 'Técnico')
    return area
  }

  dataFormat(data) {
    return moment(data).utc().format('DD/MM/YYYY')
  }

  Editar(id) {
    this.router.navigate(['/Admin/Vagas/Editar/'+id])
  }

  Inscricoes(id) {
    this.router.navigate(['/Admin/Vagas/Visualizar/'+id])
  }

  async Excluir(id) {  
    this.mensagem = await this.setAtivar(id)
    this.messageService.sendMessage(this.mensagem, 'vagas')
    this.ngOnInit()
  }

  setaDadosModal(id) {
    this.id_vaga = id
  }

  setAtivar(id) {
    return new Promise((resolve, reject) => {
      let newForm = {
        ativar: 0
      }

      this._apiVaga.patchVaga(newForm, id)
        .subscribe((res: any) => {   
          resolve("Vaga excluída com sucesso!")
        }, err => {
          reject(err)
        })
    })
  }

}
