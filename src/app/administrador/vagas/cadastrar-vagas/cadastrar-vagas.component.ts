import { VagasService } from './../../../modelos/vagas/vagas.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CampusService } from 'src/app/modelos/campus/campus.service';
import { MessageService } from 'src/app/message.service';
import { CargosService } from 'src/app/modelos/cargos/cargos.service';

@Component({
  selector: 'app-cadastrar-vagas',
  templateUrl: './cadastrar-vagas.component.html',
  styleUrls: ['./cadastrar-vagas.component.scss']
})
export class CadastrarVagasComponent implements OnInit {
  alerta_de_vagas = true
  alerta_erro: boolean = false
  mensagem: any = false
  mensagem_error: any = false
  definir_cargo = false
  dataSourceCampus: any
  dataSourceCargo: any
  vagasForm: FormGroup;

  constructor(private router: Router, private formBuilder: FormBuilder, private _apiCargo: CargosService,
    private _apiCampus: CampusService, private messageService: MessageService, private _apiVaga: VagasService) { }

  ngOnInit() {
    this.criarFormulario()

    this._apiCampus.getCampus()
    .subscribe((res:any) => {
      this.dataSourceCampus = res.data;
    }, err => {
      console.log(err);
    });      
  }
  
  criarFormulario() {
    this.vagasForm = this.formBuilder.group({
      'campus': [null, Validators.required],
      'area': [null, Validators.required],
      'cargo': [null, Validators.required]
    })    
  }

  onChangeArea(deviceValue) {
    this.definir_cargo = true

    this._apiCargo.getCargoArea(deviceValue)
    .subscribe((res:any) => {
      this.dataSourceCargo = res.data
    }, err => {
      console.log(err)
    })
  }


  postVagas(data) {
    let form = {
      campus: data.campus,
      cargo: data.cargo
    }

    this._apiVaga.postVaga(form)
      .subscribe(res => {
          this.alerta_erro = false
          console.log(res)
          this.messageService.sendMessage('Vaga cadastrada com Sucesso!', 'vagas')
          this.router.navigate(['/Admin/Vagas']);
        }, (err) => {
          console.log(err);
          this.alerta_erro = true
        });
  }

}
