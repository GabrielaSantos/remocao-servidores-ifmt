import { Component, OnInit } from '@angular/core';
import { CampusService } from 'src/app/modelos/campus/campus.service';
import { MessageService } from 'src/app/message.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-campus',
  templateUrl: './campus.component.html',
  styleUrls: ['./campus.component.scss']
})
export class CampusComponent implements OnInit {
  itemsPerPage = 20
  currentPage: number = 1;
  dataSourceCampus:any
  alerta_de_campus: boolean
  mensagem: any
  id_campus: number

  constructor(private router: Router, private _apiCampus: CampusService,  private messageService: MessageService) { }

  async ngOnInit() {
    try {
      this.mensagem = this.messageService.getMessage('campus')
      this.messageService.clearMessages('campus')
      this.dataSourceCampus = await this.getCampus()
    }
    catch (err) {
      console.error(err)
    }
  }


  getCampus() {
    return new Promise((resolve, reject) => {
      this._apiCampus.getCampus()
        .subscribe((res: any) => {
          this.alerta_de_campus = (res.data.length > 0 ? false : true)
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  CampusProximo(id) {
    this.router.navigate(['/Admin/Campus-Proximo/'+id])
  }

  Editar(id) {
    this.router.navigate(['/Admin/Campus/Editar/'+id])
  }

  Excluir(id) {
    this.id_campus = id
  }

  EnviarExclusao() {
    let form = {
      ativar: 0
    }

    this._apiCampus.patchCampus(form, this.id_campus)
      .subscribe(res => {
        this.messageService.sendMessage('Campus Excluído com Sucesso!', 'campus')
        this.ngOnInit()
      }, (err) => {
        console.log(err);
      });
  }
}

