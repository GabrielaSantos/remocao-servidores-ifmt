import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CampusService } from 'src/app/modelos/campus/campus.service';
import { CampusProximoService } from 'src/app/modelos/campus-proximo/campus-proximo.service';
import { MessageService } from 'src/app/message.service';

@Component({
  selector: 'app-campus-proximo-cadastrar',
  templateUrl: './campus-proximo-cadastrar.component.html',
  styleUrls: ['./campus-proximo-cadastrar.component.scss']
})
export class CampusProximoCadastrarComponent implements OnInit {
  dataSource:any
  dataSourceCampus:any
  alerta_de_campus: boolean
  mensagem: any
  campusForm: FormGroup
  id_campus: number
  submitted = false;

  constructor(private route: ActivatedRoute, private router: Router, private _apiCampus: CampusService, 
     private _apiCampusProximo: CampusProximoService,  private messageService: MessageService, private formBuilder: FormBuilder) { }

  async ngOnInit() {
    try {
      this.route.params.subscribe(params => {
        this.id_campus = +params['id']; // (+) converts string 'id' to a number
      })

      this.criarFormulario()
     
      this.dataSourceCampus = await this.getListaCampus()
      this.dataSource = await this.trocarCampus()
    }
    catch (err) {
      console.error(err)
    }
  }

  get f() { return this.campusForm.controls; }

  trocarCampus() {   
    let array = {}

    array =  this.dataSourceCampus.filter((item) => {
      return (item.id != this.id_campus); 
    })
    return array

  }
  
  criarFormulario() {
    this.campusForm = this.formBuilder.group({
      'campus_destino': [null, Validators.required],
      'distancia': [null, Validators.compose([Validators.required, Validators.pattern("^[0-9]*$")])]
    })    
  }


  getListaCampus() {
    return new Promise((resolve, reject) => {
      this._apiCampus.getCampus()
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  CampusProximo(id) {
    this.router.navigate(['/Admin/Campus-Proximo/'+this.id_campus])
  }

  enviarCampus(data) {
    this.submitted = true;

    // stop here if form is invalid
    if (this.campusForm.invalid) {
        return;
    }
    else {
      let form = {
        campus_origem: this.id_campus,
        campus_destino: data.campus_destino,
        distancia: data.distancia
      }

      this._apiCampusProximo.postCampusProximo(form)
      .subscribe(res => {
          this.messageService.sendMessage('Campus Próximo cadastrado com Sucesso!', 'campus-proximo')
          this.router.navigate(['/Admin/Campus-Proximo/'+this.id_campus]);
        }, (err) => {
          console.log(err);
        });  
    }
  }

}
