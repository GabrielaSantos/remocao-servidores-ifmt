import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampusProximoCadastrarComponent } from './campus-proximo-cadastrar.component';

describe('CampusProximoCadastrarComponent', () => {
  let component: CampusProximoCadastrarComponent;
  let fixture: ComponentFixture<CampusProximoCadastrarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampusProximoCadastrarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampusProximoCadastrarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
