import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CampusService } from 'src/app/modelos/campus/campus.service';
import { CampusProximoService } from 'src/app/modelos/campus-proximo/campus-proximo.service';
import { MessageService } from 'src/app/message.service';

@Component({
  selector: 'app-campus-proximo-editar',
  templateUrl: './campus-proximo-editar.component.html',
  styleUrls: ['./campus-proximo-editar.component.scss']
})
export class CampusProximoEditarComponent implements OnInit {

  dataSource:any
  dataSourceCampus:any
  dataSourceCampusProximo: any
  alerta_de_campus: boolean
  mensagem: any
  campusForm: FormGroup
  id_campus: number
  submitted = false;

  constructor(private route: ActivatedRoute, private router: Router, private _apiCampus: CampusService, 
     private _apiCampusProximo: CampusProximoService,  private messageService: MessageService, private formBuilder: FormBuilder) { }

  async ngOnInit() {
    try {
      this.route.params.subscribe(params => {
        this.id_campus = +params['id']; // (+) converts string 'id' to a number
      })

      this.criarFormulario()
     
      this.dataSourceCampus = await this.getListaCampus()
      this.dataSourceCampusProximo = await this.getListaCampusProximo(this.id_campus)
      await this.setDadosForm(this.dataSourceCampusProximo[0])
      this.dataSource = await this.trocarCampus()
    }
    catch (err) {
      console.error(err)
    }
  }

  get f() { return this.campusForm.controls; }

  trocarCampus() {   
    let array = {}

    array =  this.dataSourceCampus.filter((item) => {
      return (item.id != this.dataSourceCampusProximo[0].campus_origem); 
    })
    return array

  }
  
  criarFormulario() {
    this.campusForm = this.formBuilder.group({
      'campus_destino': [null, Validators.required],
      'distancia': [null, Validators.compose([Validators.required, Validators.pattern("^[0-9]*$")])]
    })    
  }

  setDadosForm(dados) {
    this.campusForm.setValue({
      campus_destino: dados.campus_destino,      
      distancia: dados.distancia,
    })
  }


  getListaCampus() {
    return new Promise((resolve, reject) => {
      this._apiCampus.getCampus()
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  getListaCampusProximo(id) {
    return new Promise((resolve, reject) => {
      this._apiCampusProximo.getCampusProximo(id)
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  CampusProximo(id) {
    this.router.navigate(['/Admin/Campus-Proximo/'+this.dataSourceCampusProximo[0].campus_origem])
  }

  enviarCampus(data) {
    this.submitted = true;

    // stop here if form is invalid
    if (this.campusForm.invalid) {
        return;
    }
    else {
      let form = {
        campus_destino: data.campus_destino,
        distancia: data.distancia
      }

      this._apiCampusProximo.patchCampusProximo(form, this.id_campus)
      .subscribe(res => {
          this.messageService.sendMessage('Campus Próximo editado com Sucesso!', 'campus-proximo')
          this.router.navigate(['/Admin/Campus-Proximo/'+this.dataSourceCampusProximo[0].campus_origem]);
        }, (err) => {
          console.log(err);
        });  
    }
  }

}
