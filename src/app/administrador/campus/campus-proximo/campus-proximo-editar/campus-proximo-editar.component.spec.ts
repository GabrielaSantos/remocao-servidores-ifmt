import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampusProximoEditarComponent } from './campus-proximo-editar.component';

describe('CampusProximoEditarComponent', () => {
  let component: CampusProximoEditarComponent;
  let fixture: ComponentFixture<CampusProximoEditarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampusProximoEditarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampusProximoEditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
