import { CampusProximoService } from './../../../modelos/campus-proximo/campus-proximo.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CampusService } from 'src/app/modelos/campus/campus.service';
import { MessageService } from 'src/app/message.service';

@Component({
  selector: 'app-campus-proximo',
  templateUrl: './campus-proximo.component.html',
  styleUrls: ['./campus-proximo.component.scss']
})
export class CampusProximoComponent implements OnInit {
  itemsPerPage = 20
  currentPage: number = 1;
  dataSourceCampusProximo:any
  dataSourceCampus:any
  alerta_de_campus: boolean
  mensagem: any
  id_campus: number
  id_campus_excluir: number

  constructor(private route: ActivatedRoute, private router: Router, private _apiCampus: CampusService,  private _apiCampusProximo: CampusProximoService,  private messageService: MessageService) { }

  async ngOnInit() {
    try {
      this.route.params.subscribe(params => {
        this.id_campus = +params['id']; // (+) converts string 'id' to a number
      })

      this.mensagem = this.messageService.getMessage('campus-proximo')
      this.messageService.clearMessages('campus-proximo')
      this.dataSourceCampusProximo = await this.getCampusProximo(this.id_campus)

      if(!this.alerta_de_campus) {
        this.dataSourceCampus = await this.getCampus()
        await this.setDados()
      }
    }
    catch (err) {
      console.error(err)
    }
  }

  setDados() {
    this.dataSourceCampusProximo.forEach((element, i) => {
      this.dataSourceCampusProximo[i].campus_origem = this.setNomeCampus(element.campus_origem)
      this.dataSourceCampusProximo[i].campus_destino = this.setNomeCampus(element.campus_destino)
     })
  }

  setNomeCampus(campus) {
    let array = {}
    array =  this.dataSourceCampus.filter((item) => {
      return (item.id == campus); 
    })
    return array[0].nome
  }

  cadastrar() {
    this.router.navigate(['/Admin/Campus-Proximo/Cadastrar/'+this.id_campus])
  }

  Editar(id) {
    this.router.navigate(['/Admin/Campus-Proximo/Editar/'+id])
  }

  Excluir(id) {
    this.id_campus_excluir = id
  }

  EnviarExclusao() {
    let form = {
      ativar: 0
    }

    this._apiCampusProximo.patchCampusProximo(form, this.id_campus_excluir)
    .subscribe(res => {
        this.messageService.sendMessage('Campus Próximo excluído com Sucesso!', 'campus-proximo')
        this.ngOnInit()
      }, (err) => {
        console.log(err);
      });  
  }

  getCampus() {
    return new Promise((resolve, reject) => {
      this._apiCampus.getCampus()
        .subscribe((res: any) => {          
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  getCampusProximo(id) {
    return new Promise((resolve, reject) => {
      this._apiCampusProximo.getCampusProximoID(id)
        .subscribe((res: any) => {
          this.alerta_de_campus = (res.data.length > 0 ? false : true)
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }


}
