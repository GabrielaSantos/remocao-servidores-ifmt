import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampusProximoComponent } from './campus-proximo.component';

describe('CampusProximoComponent', () => {
  let component: CampusProximoComponent;
  let fixture: ComponentFixture<CampusProximoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampusProximoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampusProximoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
