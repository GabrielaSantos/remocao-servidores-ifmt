import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CampusService } from 'src/app/modelos/campus/campus.service';
import { MessageService } from 'src/app/message.service';

@Component({
  selector: 'app-cadastrar-campus',
  templateUrl: './cadastrar-campus.component.html',
  styleUrls: ['./cadastrar-campus.component.scss']
})
export class CadastrarCampusComponent implements OnInit {
  dataSourceCampus: any
  campusForm: FormGroup

  constructor(private router: Router, private formBuilder: FormBuilder, 
    private _apiCampus: CampusService, private messageService: MessageService) { }

    ngOnInit() {
      this.criarFormulario()    
    }

    criarFormulario() {
      this.campusForm = this.formBuilder.group({
        'nome': [null, Validators.required]
      })    
    }

    enviarCampus(data) {
      let form = {
        nome: data.nome
      }

      console.log(form)
  
      this._apiCampus.postCampus(form)
        .subscribe(res => {
            this.messageService.sendMessage('Campus Cadastrado com Sucesso!', 'campus')
            this.router.navigate(['/Admin/Campus']);
          }, (err) => {
            console.log(err);
          });
    }

}
