import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastrarCampusComponent } from './cadastrar-campus.component';

describe('CadastrarCampusComponent', () => {
  let component: CadastrarCampusComponent;
  let fixture: ComponentFixture<CadastrarCampusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastrarCampusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastrarCampusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
