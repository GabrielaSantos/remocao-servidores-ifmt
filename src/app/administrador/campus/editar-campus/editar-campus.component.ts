import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CampusService } from 'src/app/modelos/campus/campus.service';
import { MessageService } from 'src/app/message.service';

@Component({
  selector: 'app-editar-campus',
  templateUrl: './editar-campus.component.html',
  styleUrls: ['./editar-campus.component.scss']
})
export class EditarCampusComponent implements OnInit {
  id_campus: number
  dataSourceCampus: any
  campusForm: FormGroup

  constructor(private route: ActivatedRoute, private router: Router, private formBuilder: FormBuilder, 
    private _apiCampus: CampusService, private messageService: MessageService) { }


    async ngOnInit() {
      this.route.params.subscribe(params => {
        this.id_campus = +params['id']; // (+) converts string 'id' to a number
      })
  
      this.criarFormulario()
  
      try {
        this.dataSourceCampus = await this.getCampus(this.id_campus)
        await this.setDadosCampus(this.dataSourceCampus[0])   
      }
      catch (err) {
        console.error(err)
      }
    }

    setDadosCampus(dados) {
      this.campusForm.setValue({
        nome: dados.nome  
      })
    }

    criarFormulario() {
      this.campusForm = this.formBuilder.group({
        'nome': [null, Validators.required]
      })    
    }

    getCampus(id) {
      return new Promise((resolve, reject) => {
        this._apiCampus.getCampusID(id)
          .subscribe((res: any) => {
            resolve(res.data)
          }, err => {
            reject(err)
          })
      })
    }

    enviarCampus(data) {
      let form = {
        nome: data.nome
      }

      this._apiCampus.patchCampus(form, this.id_campus)
        .subscribe(res => {
          this.messageService.sendMessage('Campus Editado com Sucesso!', 'campus')
          this.router.navigate(['/Admin/Campus']);
        }, (err) => {
          console.log(err);
        });
    }
}
