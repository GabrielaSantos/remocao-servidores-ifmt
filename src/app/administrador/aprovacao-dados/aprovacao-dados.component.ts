import { Component, OnInit } from '@angular/core';
import { DadosService, Usuario } from 'src/app/modelos/inscricoes/dados.service';
import { MessageService } from 'src/app/message.service';
import * as moment from 'moment';
import { Router } from '@angular/router';


@Component({
  selector: 'app-aprovacao-dados',
  templateUrl: './aprovacao-dados.component.html',
  styleUrls: ['./aprovacao-dados.component.scss']
})
export class AprovacaoDadosComponent implements OnInit {
  alerta_de_dados = true
  dataSource: Usuario[] = [];
  usuario = {}
  mensagem: any
  mensagem_error: any
  itemsPerPage = 20
  currentPage: number = 1;

  constructor(private _api: DadosService, private messageService: MessageService, private router: Router) { }

  ngOnInit() {
    this.mensagem = this.messageService.getMessage('admin-dados')
    this.messageService.clearMessages('admin-dados')

    this.mensagem_error = this.messageService.getMessage('admin-dados-error')
    this.messageService.clearMessages('admin-dados-error')

    this._api.getUsuariosDados(0)
      .subscribe((res: any) => {
        this.dataSource = res.data
        if(this.dataSource.length > 0){
          this.alerta_de_dados = false
          this.formatarDados()
        }
      }, err => {
        console.log(err);
      })
  }

  formatarDados() {
    this.dataSource.forEach((element, i) => {
      this.usuario[i] = element  
    })    
  }

  Aprovar(event) {
    this.messageService.sendMessage(event.id, 'aprovar_dados')
    this.router.navigate(['/Admin/AvaliarDados/'+event.id])
  }

  dataFormat(data) {
    return moment(data).utc().format('DD/MM/YYYY')
  }

}
