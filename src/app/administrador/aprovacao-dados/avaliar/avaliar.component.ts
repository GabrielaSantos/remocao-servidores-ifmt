import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms'
import * as moment from 'moment';

import { DadosService } from 'src/app/modelos/inscricoes/dados.service'
import { CargosService } from 'src/app/modelos/cargos/cargos.service'
import { MensagensService } from 'src/app/modelos/mensagens/mensagens.service'
import { MessageService } from 'src/app/message.service';
import { CampusService, Campus } from 'src/app/modelos/campus/campus.service';


@Component({
  selector: 'app-avaliar',
  templateUrl: './avaliar.component.html',
  styleUrls: ['./avaliar.component.scss']
})
export class AvaliarComponent implements OnInit {
  _id: String = ''
  login = 2015178440085
  login_id = 2
  aprovacao = {}

  id_user: any
  mensagem: any
  dados: any
  dataSourceUsuario: any
  dataSourceCampus:any
  dataSourceCargo: any
  dataSourceArea: any

  campus: string
  cargo: string
  area: string

  usuarioForm: FormGroup
  aprovacaoForm: FormGroup

  constructor(private router: Router, private _apiCampus: CampusService, private messageService: MessageService,
    private formBuilder: FormBuilder, private _apiUsuario: DadosService, private route: ActivatedRoute,
    private _apiCargo: CargosService, private _apiMensagem: MensagensService) { }


  async ngOnInit() {
    this.route.params.subscribe(params => {
      this.id_user = +params['id']; // (+) converts string 'id' to a number
    })

    this.criarFormulario()

    try {
      this.dataSourceUsuario = await this.getUsuario()

      this.dataSourceCampus = await this.getCampus()
      this.campus = this.dataSourceCampus[0].nome

      this.dataSourceArea = await this.getArea(this.dataSourceUsuario[0].cargo)
      this.area = (this.dataSourceArea == 1 ? 'Professor do Ensino Básico, Técnico e Tecnológico' : 'Técnico')


      this.dataSourceCargo = await this.getCargo(this.dataSourceUsuario[0].cargo)
      this.cargo = this.dataSourceCargo[0].nome

      this.setDadosPessoais(this.dataSourceUsuario[0])
    }

    catch (err) {
      console.error(err)
    }
  }

   getUsuario() {
    return new Promise((resolve, reject) => {
      this._apiUsuario.getUsuarioID(this.id_user)
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  getCampus() {
    return new Promise((resolve, reject) => {
      this._apiCampus.getCampus()
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  getArea(cargoAtual) {
    return new Promise((resolve, reject) => {
      this._apiCargo.getCargoID(cargoAtual)
        .subscribe((res: any) => {
          resolve(res.data[0].area)
        }, err => {
          reject(err)
        })
    })
  }

  getCargo(cargo) {
    return new Promise((resolve, reject) => {
      this._apiCargo.getCargoID(cargo)
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  criarFormulario() {
    this.usuarioForm = this.formBuilder.group({
      'nome': [null, Validators.required],
      'matricula': [null, Validators.required],
      'email': [null, Validators.required],
      'telefone': [null, Validators.required],
      'nascimento': [null, Validators.required],
      'campus_atual': [null, Validators.required],
      'data_ingresso_campus': [null, Validators.required],
      'data_ingresso_ifmt': [null, Validators.required],
      'area': [null, Validators.required],
      'cargo': [null, Validators.required]
    })

    this.aprovacaoForm = this.formBuilder.group({
      'observacoes_avaliacao': [null],
      'avaliar': [null]
    })
  }

  setDadosPessoais(dados) {
    this.usuarioForm.setValue({
      nome: dados.nome,
      matricula: dados.matricula,
      email: dados.email,
      telefone: dados.telefone,
      nascimento: {
        day: Number.parseInt(moment(dados.nascimento).utc().format('DD')),
        month: Number.parseInt(moment(dados.nascimento).utc().format('MM')),
        year: Number.parseInt(moment(dados.nascimento).utc().format('YYYY'))
      },
      campus_atual: dados.campus_atual,
      data_ingresso_campus: {
        day: Number.parseInt(moment(dados.data_ingresso_campus).utc().format('DD')),
        month: Number.parseInt(moment(dados.data_ingresso_campus).utc().format('MM')),
        year: Number.parseInt(moment(dados.data_ingresso_campus).utc().format('YYYY'))
      },
      data_ingresso_ifmt: {
        day: Number.parseInt(moment(dados.data_ingresso_ifmt).utc().format('DD')),
        month: Number.parseInt(moment(dados.data_ingresso_ifmt).utc().format('MM')),
        year: Number.parseInt(moment(dados.data_ingresso_ifmt).utc().format('YYYY'))
      },
      area: this.area,
      cargo: dados.cargo
    })
  }

  enviarDados(form: any) {
    this.aprovacao = {
      aprovacao_dados: (form.avaliar == 0 ? 2 : 1),
    }
    
    if(form.observacoes_avaliacao != null) {
      this.mensagem = {
        usuario: this.id_user,
        avaliador: this.login_id,
        tipo: 1,
        mensagem: form.observacoes_avaliacao
      }
  
      this._apiMensagem.postMensagem(this.mensagem)
      .subscribe(res => {
        console.log("Mensagem cadastrada!")
        this.patchUsuario()
      }, (err) => {
        console.log(err);
        this.messageService.sendMessage('Erro ao Avaliar os Dados!', 'admin-dados-error')
        this.router.navigate(['/Admin']);
      });  
    }
    else {
      this.patchUsuario()
    }

    
  }

  patchUsuario() {
    this._apiUsuario.patchUsuario(this.aprovacao, this.id_user)
    .subscribe(res => {
      this.messageService.sendMessage('Dados Avaliados com Sucesso!', 'admin-dados')
      this.router.navigate(['/Admin']);
    }, (err) => {
      console.log(err);
    });
  }
}
