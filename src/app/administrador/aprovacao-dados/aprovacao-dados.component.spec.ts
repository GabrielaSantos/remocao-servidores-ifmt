import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AprovacaoDadosComponent } from './aprovacao-dados.component';

describe('AprovacaoDadosComponent', () => {
  let component: AprovacaoDadosComponent;
  let fixture: ComponentFixture<AprovacaoDadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AprovacaoDadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AprovacaoDadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
