import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { MessageService } from 'src/app/message.service';
import { BloqueiosService } from 'src/app/modelos/bloqueios/bloqueios.service';
import { DadosService, Usuario } from 'src/app/modelos/inscricoes/dados.service';

@Component({
  selector: 'app-bloqueios',
  templateUrl: './bloqueios.component.html',
  styleUrls: ['./bloqueios.component.scss']
})
export class BloqueiosComponent implements OnInit {
  alerta_de_bloqueios = true
  mensagem: any
  id_bloqueio: any
  itemsPerPage = 20
  currentPage: number = 1;

  dataSource: any
  dataSourceBloqueio: any
  dataSourceUsuario: any

  constructor(private router: Router, private messageService: MessageService, private _apiBloqueio: BloqueiosService,
    private _apiUsuario: DadosService) { }

  async ngOnInit() {
    this.mensagem = this.messageService.getMessage('bloqueios')
    this.messageService.clearMessages('bloqueios')

    try {
      this.dataSource = await this.getListaBloqueios()

      if(!this.alerta_de_bloqueios) {
        this.dataSourceUsuario = await this.getUsuario()
        this.dataSourceBloqueio = await this.setDados(this.dataSource)
      }
    } 

    catch (err) {
      console.error(err)
    }
  }

  getListaBloqueios() {
    return new Promise((resolve, reject) => {
      this._apiBloqueio.getBloqueios()
        .subscribe((res: any) => {
          if(res.data.length > 0) {
            this.alerta_de_bloqueios = false
          }
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  getUsuario() {
    return new Promise((resolve, reject) => {
      this._apiUsuario.getUsuarios()
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  setDados(dados) {
    let array = dados     
    this.dataSource.forEach((element, i) => {
      array[i].usuario = this.setUsuario(element.usuario)
    })
    return array
  }

  setUsuario(id) {
    let array = this.dataSourceUsuario.filter((item) => {
        return (item.id == id); 
    }) 
    return array[0]
    
  }

  Editar(id) {
    this.router.navigate(['/Admin/Bloqueios/Editar/'+id])
  }

  async Excluir(id) {  
    let newForm = {
      ativar: 0
    }     

    await this.setBloqueio(newForm, id)

    this.messageService.sendMessage("Bloqueio Excluído", 'bloqueios')
    this.ngOnInit()
  }

  setaDadosModal(id) {
    this.id_bloqueio = id
  }

  setBloqueio(array, id) {
    return new Promise((resolve, reject) => {
      this._apiBloqueio.patchBloqueio(array, id)
        .subscribe((res: any) => {   
          resolve(res)
        }, err => {
          reject(err)
        })
    })
  }

   dataFormat(data) {
    return moment(data).utc().format('DD/MM/YYYY')
  }

}
