import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarBloqueioComponent } from './editar-bloqueio.component';

describe('EditarBloqueioComponent', () => {
  let component: EditarBloqueioComponent;
  let fixture: ComponentFixture<EditarBloqueioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarBloqueioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarBloqueioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
