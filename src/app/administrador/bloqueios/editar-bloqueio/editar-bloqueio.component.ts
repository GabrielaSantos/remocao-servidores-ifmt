import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from 'src/app/message.service';
import { DadosService } from 'src/app/modelos/inscricoes/dados.service';
import { CargosService } from 'src/app/modelos/cargos/cargos.service';
import { BloqueiosService } from 'src/app/modelos/bloqueios/bloqueios.service';
import { CampusService } from 'src/app/modelos/campus/campus.service';
import { RemocaoService } from 'src/app/modelos/inscricoes/remocao.service';
import * as moment from 'moment';

@Component({
  selector: 'app-editar-bloqueio',
  templateUrl: './editar-bloqueio.component.html',
  styleUrls: ['./editar-bloqueio.component.scss']
})
export class EditarBloqueioComponent implements OnInit {
  id_bloqueio: number
  cargo: string
  area: string
  usuarioForm: FormGroup
  pesquisaUser: FormGroup
  bloqueioForm: FormGroup
  dataSourceBloqueio: any
  dataSourceUsuario: any
  dataSourceCampus:any
  dataSourceCargo: any
  dataSourceArea: any
  
  constructor(private router: Router, private messageService: MessageService, private formBuilder: FormBuilder, private _apiUsuario: DadosService, private route: ActivatedRoute,
    private _apiCargo: CargosService, private _apiBloqueios: BloqueiosService,
    private _apiCampus: CampusService, private _apiInscricao: RemocaoService) { }

  async ngOnInit() {
    this.route.params.subscribe(params => {
      this.id_bloqueio = +params['id']; // (+) converts string 'id' to a number
    })
    this.criarFormulario()

    try {
      this.dataSourceBloqueio = await this.getBloqueio(this.id_bloqueio)
      this.dataSourceUsuario = await this.getUsuario(this.dataSourceBloqueio[0].usuario)
      this.dataSourceCampus = await this.getCampus(this.dataSourceUsuario.campus_atual)
      this.dataSourceCargo = await this.getCargo(this.dataSourceUsuario.cargo)
      this.cargo = this.dataSourceCargo[0].nome
      this.area = (this.dataSourceCargo[0] == 1 ? 'Professor do Ensino Básico, Técnico e Tecnológico' : 'Técnico')
      await this.setDadosFormulario(this.dataSourceUsuario, this.dataSourceBloqueio[0])
    }
    catch (err) {
      console.error(err)
    }
  }

  async enviarDados(dados) {
    
    try {
      let newForm = {
        justificativa: dados.justificativa,
        data_liberacao: dados.data_liberacao.year+'-'+dados.data_liberacao.month+'-'+dados.data_liberacao.day
      }     

      await this.setBloqueio(newForm, this.id_bloqueio)

      this.messageService.sendMessage("Bloqueio do Servidor(a) "+ this.dataSourceUsuario.nome +" Editado!", 'bloqueios')
      this.router.navigate(['/Admin/Bloqueios'])

    }
    catch (err) {
      console.error(err)
    }
  }

  getBloqueio(id) {
    return new Promise((resolve, reject) => {
      this._apiBloqueios.getBloqueioID(id)
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  criarFormulario() {
    this.usuarioForm = this.formBuilder.group({
      'nome': [null, Validators.required],
      'matricula': [null, Validators.required],
      'email': [null, Validators.required],
      'telefone': [null, Validators.required],
      'nascimento': [null, Validators.required],
      'campus_atual': [null, Validators.required],
      'data_ingresso_campus': [null, Validators.required],
      'data_ingresso_ifmt': [null, Validators.required],
      'area': [null, Validators.required],
      'cargo': [null, Validators.required]
    })

    this.bloqueioForm = this.formBuilder.group({
      'data_liberacao': [null, Validators.required],
      'justificativa': [null, Validators.required]
    })
  }

  getUsuario(id) {
    return new Promise((resolve, reject) => {
      this._apiUsuario.getUsuarioID(id)
        .subscribe((res: any) => {
          resolve(res.data[0])
        }, err => {
          reject(err)
        })
    })
  }

  getCampus(id) {
    return new Promise((resolve, reject) => {
      this._apiCampus.getCampusID(id)
        .subscribe((res: any) => {
          resolve(res.data[0].nome)
        }, err => {
          reject(err)
        })
    })
  }

  getCargo(cargo) {
    return new Promise((resolve, reject) => {
      this._apiCargo.getCargoID(cargo)
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  setDadosFormulario(user, bloqueio) {
    moment.locale('pt-BR');

    this.usuarioForm.setValue({
      nome: user.nome,
      matricula: user.matricula,
      email: user.email,
      telefone: user.telefone,
      nascimento: {
        day: Number.parseInt(moment(user.nascimento).utc().format('DD')),
        month: Number.parseInt(moment(user.nascimento).utc().format('MM')),
        year: Number.parseInt(moment(user.nascimento).utc().format('YYYY')),
      },
      campus_atual: this.dataSourceCampus,
      data_ingresso_campus: {
        day: Number.parseInt(moment(user.data_ingresso_campus).utc().format('DD')),
        month: Number.parseInt(moment(user.data_ingresso_campus).utc().format('MM')),
        year: Number.parseInt(moment(user.data_ingresso_campus).utc().format('YYYY')),
      },
      data_ingresso_ifmt: {
        day: Number.parseInt(moment(user.data_ingresso_ifmt).utc().format('DD')),
        month: Number.parseInt(moment(user.data_ingresso_ifmt).utc().format('MM')),
        year: Number.parseInt(moment(user.data_ingresso_ifmt).utc().format('YYYY')),
      },
      area: this.area,
      cargo: this.cargo
    })    

    this.bloqueioForm.setValue({
      justificativa: bloqueio.justificativa,
      data_liberacao: {
        day: Number.parseInt(moment(bloqueio.data_liberacao).utc().format('DD')),
        month: Number.parseInt(moment(bloqueio.data_liberacao).utc().format('MM')),
        year: Number.parseInt(moment(bloqueio.data_liberacao).utc().format('YYYY')),
      }
    })    

    console.log(moment(bloqueio.data_liberacao).utc().format('DD'))

  }

  setBloqueio(array, id) {
    return new Promise((resolve, reject) => {
      this._apiBloqueios.patchBloqueio(array, id)
        .subscribe((res: any) => {   
          resolve(res)
        }, err => {
          reject(err)
        })
    })
  }
}
