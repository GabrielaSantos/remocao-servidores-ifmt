import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastroBloqueioComponent } from './cadastro-bloqueio.component';

describe('CadastroBloqueioComponent', () => {
  let component: CadastroBloqueioComponent;
  let fixture: ComponentFixture<CadastroBloqueioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastroBloqueioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastroBloqueioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
