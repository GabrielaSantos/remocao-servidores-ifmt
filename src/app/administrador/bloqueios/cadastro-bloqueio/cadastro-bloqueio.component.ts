import { BloqueiosService } from './../../../modelos/bloqueios/bloqueios.service';
import { RemocaoService } from './../../../modelos/inscricoes/remocao.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from 'src/app/message.service';
import { DadosService } from 'src/app/modelos/inscricoes/dados.service';
import * as moment from 'moment';
import { CargosService } from 'src/app/modelos/cargos/cargos.service';
import { CampusService } from 'src/app/modelos/campus/campus.service';

@Component({
  selector: 'app-cadastro-bloqueio',
  templateUrl: './cadastro-bloqueio.component.html',
  styleUrls: ['./cadastro-bloqueio.component.scss']
})
export class CadastroBloqueioComponent implements OnInit {
  id_bloqueio: number
  cargo: string
  area: string
  usuarioForm: FormGroup
  pesquisaUser: FormGroup
  bloqueioForm: FormGroup
  dataSourceUsuario: any
  dataSourceCampus:any
  dataSourceCargo: any
  dataSourceArea: any
  usuarioSelecionado: any
  ver_usuarioSelecionado = false
  busca_usuario: boolean = false
  
  constructor(private router: Router, private messageService: MessageService, private formBuilder: FormBuilder, private _apiUsuario: DadosService, private route: ActivatedRoute,
    private _apiCargo: CargosService, private _apiBloqueios: BloqueiosService,
    private _apiCampus: CampusService, private _apiInscricao: RemocaoService) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id_bloqueio = +params['id']; // (+) converts string 'id' to a number
    })

    this.criarFormulario()

  }

  async enviarDados(dados) {
    try {
        let newForm = {
          usuario: this.usuarioSelecionado.id,
          justificativa: dados.justificativa,
          data_liberacao: dados.data_liberacao.year+'-'+dados.data_liberacao.month+'-'+dados.data_liberacao.day
        }

        let inscricao: any = await this.getIDInscricao(this.usuarioSelecionado.id)

        if(inscricao.length > 0) {
          let formInscricao = {
            ativar: 0
          }
          await this.setInscricao(formInscricao, inscricao[0].id)
        }

        await this.setBloqueio(newForm)

        this.messageService.sendMessage("Servidor(a) "+ this.usuarioSelecionado.nome +" Bloqueado(a)!", 'bloqueios')
        this.router.navigate(['/Admin/Bloqueios'])
    }
    catch (err) {
      console.error(err)
    }
  }

  async pesquisarUsuario(dados) {
    this.ver_usuarioSelecionado = false
    this.dataSourceUsuario = await this.getUsuario(dados.busca)
    this.busca_usuario = (this.dataSourceUsuario.length > 0 ? true : false)
  }

  async setUsuario(dado) {
    this.usuarioSelecionado = dado

    try {
      this.dataSourceCampus = await this.getCampus(this.usuarioSelecionado.campus_atual)
      this.dataSourceCargo = await this.getCargo(this.usuarioSelecionado.cargo)
      this.cargo = this.dataSourceCargo[0].nome
      this.area = (this.dataSourceCargo[0] == 1 ? 'Professor do Ensino Básico, Técnico e Tecnológico' : 'Técnico')
      await this.setDadosPessoais(this.usuarioSelecionado)
      this.ver_usuarioSelecionado = true
    }
    catch (err) {
      console.error(err)
    }
  }

  criarFormulario() {
    this.usuarioForm = this.formBuilder.group({
      'nome': [null, Validators.required],
      'matricula': [null, Validators.required],
      'email': [null, Validators.required],
      'telefone': [null, Validators.required],
      'nascimento': [null, Validators.required],
      'campus_atual': [null, Validators.required],
      'data_ingresso_campus': [null, Validators.required],
      'data_ingresso_ifmt': [null, Validators.required],
      'area': [null, Validators.required],
      'cargo': [null, Validators.required]
    })

    this.pesquisaUser = this.formBuilder.group({
      'busca': [null]
    })

    this.bloqueioForm = this.formBuilder.group({
      'data_liberacao': [null, Validators.required],
      'justificativa': [null, Validators.required]
    })
  }

  getUsuario(dados) {
    return new Promise((resolve, reject) => {
      this._apiUsuario.getUsuarioLike(dados)
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  getCampus(id) {
    return new Promise((resolve, reject) => {
      this._apiCampus.getCampusID(id)
        .subscribe((res: any) => {
          resolve(res.data[0].nome)
        }, err => {
          reject(err)
        })
    })
  }

  getCargo(cargo) {
    return new Promise((resolve, reject) => {
      this._apiCargo.getCargoID(cargo)
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  setDadosPessoais(dados) {
    this.usuarioForm.setValue({
      nome: dados.nome,
      matricula: dados.matricula,
      email: dados.email,
      telefone: dados.telefone,
      nascimento: {
        day: Number.parseInt(moment(dados.nascimento).utc().format('DD')),
        month: Number.parseInt(moment(dados.nascimento).utc().format('MM')),
        year: Number.parseInt(moment(dados.nascimento).utc().format('YYYY')),
      },
      campus_atual: this.dataSourceCampus,
      data_ingresso_campus: {
        day: Number.parseInt(moment(dados.data_ingresso_campus).utc().format('DD')),
        month: Number.parseInt(moment(dados.data_ingresso_campus).utc().format('MM')),
        year: Number.parseInt(moment(dados.data_ingresso_campus).utc().format('YYYY')),
      },
      data_ingresso_ifmt: {
        day: Number.parseInt(moment(dados.data_ingresso_ifmt).utc().format('DD')),
        month: Number.parseInt(moment(dados.data_ingresso_ifmt).utc().format('MM')),
        year: Number.parseInt(moment(dados.data_ingresso_ifmt).utc().format('YYYY')),
      },
      area: this.area,
      cargo: this.cargo
    })    
  }

  getIDInscricao(id) {
    return new Promise((resolve, reject) => {    
      this._apiInscricao.getUltimaInscricao(id)
        .subscribe((res: any) => {   
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  setInscricao(form, id) {
    return new Promise((resolve, reject) => {    
      this._apiInscricao.patchInscricao(form, id)
        .subscribe((res: any) => {   
          resolve(res)
        }, err => {
          reject(err)
        })
    })
  }

  setBloqueio(array) {
    return new Promise((resolve, reject) => {
      this._apiBloqueios.postBloqueios(array)
        .subscribe((res: any) => {   
          resolve(res)
        }, err => {
          reject(err)
        })
    })
  }
}
