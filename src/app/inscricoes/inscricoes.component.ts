import { Component, OnInit } from '@angular/core';
import { Usuario, DadosService } from '../modelos/inscricoes/dados.service';
import { MessageService } from '../message.service';
import { LoginService } from '../modelos/login/login.service';

@Component({
  selector: 'app-inscricoes',
  templateUrl: './inscricoes.component.html',
  styleUrls: ['./inscricoes.component.scss']
})
export class InscricoesComponent implements OnInit {
  mensagem = ''
  dados_aprovados = 0
  cadastro_dados = false
  visualizar_dados = false
  alerta_de_dados = "Você não realizou o cadastro de Dados!"
  login: any

  dataSource: any;

  constructor(private _api: DadosService, private messageService: MessageService, 
     private _apiLogin: LoginService) { }

  ngOnInit() {
    this.mensagem = this.messageService.getMessage('inscricoes')
    this.messageService.clearMessages('inscricoes')

    this.login = this.getLogin()

    this._api.getUsuario(this.login.matricula)
      .subscribe((res: any) => {
        this.dataSource = res.data
        this.getDados()
      }, err => {
        console.log(err);
      })     
  }

  getLogin() {
    return this._apiLogin.getLogin()
  }

  getDados() {
    if (this.dataSource[0]) {
      this.visualizar_dados = true
      this.messageService.sendMessage(this.dataSource[0], 'visualizar_dados')
      if (this.dataSource[0].aprovacao_dados == 0) {
        this.cadastro_dados = true
        this.alerta_de_dados = "Aguardando aprovação de dados!"
      }
      else if (this.dataSource[0].aprovacao_dados == 2) {
        this.cadastro_dados = true
        this.alerta_de_dados = "Atualize suas informações conforme comentário de avaliação e reencaminhe para análise."
      }
      else {
        this.dados_aprovados = 1;
      }
    }
    else {
      this.cadastro_dados = false
      this.alerta_de_dados = "Você não realizou o cadastro de Dados!"
    }
  }
}
