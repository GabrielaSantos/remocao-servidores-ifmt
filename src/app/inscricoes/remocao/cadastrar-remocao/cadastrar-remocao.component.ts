import { Component, OnInit } from '@angular/core';
import Stepper from 'bs-stepper';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import {  CampusService } from 'src/app/modelos/campus/campus.service';
import { MessageService } from 'src/app/message.service';
import { LoginService } from 'src/app/modelos/login/login.service';
import { RemocaoService } from 'src/app/modelos/inscricoes/remocao.service';

@Component({
  selector: 'app-cadastrar-remocao',
  templateUrl: './cadastrar-remocao.component.html',
  styleUrls: ['./cadastrar-remocao.component.scss']
})
export class CadastrarRemocaoComponent implements OnInit {
  name = 'Angular';
  private stepper: Stepper;

  login: any
  InscricaoForm: FormGroup;
  alerta_erro: boolean = false
  dataSourceCampus: any
  dataSourceRemocao: any

  
  constructor(private formBuilder: FormBuilder, private _apiCampus: CampusService, 
    private messageService: MessageService, private router: Router,
    private _apiInscricao: RemocaoService, private _apiLogin: LoginService) { }

  next() {
    this.stepper.next();
  }
  
  async ngOnInit() {
    this.stepper = new Stepper(document.querySelector('#stepper1'), {
      linear: false,
      animation: true
    })
    this.criarFormulario()

    try {
      this.login = await this.getLogin()
      this.dataSourceRemocao = await this.getInscricao()
      this.dataSourceCampus = await this.getCampus()
    }

    catch (err) {
      console.error(err)
    }
   
  }

  getInscricao() {
    return new Promise((resolve, reject) => {
      this._apiInscricao.getInscricao(this.login.id)
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  getCampus() {
    return new Promise((resolve, reject) => {
      this._apiCampus.getCampus()
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }


  getLogin() {
    return new Promise((resolve, reject) => {
      resolve(this._apiLogin.getLogin())
    })
  }

  postInscricao(data: any) {
    let form = {}

    form['campus'] = data.value['campus']
    form['usuario'] =  this.login.id

    this._apiInscricao.postInscricao(form)
    .subscribe(res => {
      this.alerta_erro = false
      this.messageService.sendMessage('Cadastro para Remoção Permanente realizado com sucesso!', 'inscricoes')
      this.router.navigate(['/Inscricoes']);
    }, (err) => {
      console.log(err);
      this.alerta_erro = true
    });

  }

  criarFormulario() {
    this.InscricaoForm = this.formBuilder.group({
      'confirmacao': [null, Validators.required],
      'campus': [null, Validators.required]
    })    
  }
}
