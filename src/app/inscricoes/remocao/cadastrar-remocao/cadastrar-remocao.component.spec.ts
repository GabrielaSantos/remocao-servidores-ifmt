import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastrarRemocaoComponent } from './cadastrar-remocao.component';

describe('CadastrarRemocaoComponent', () => {
  let component: CadastrarRemocaoComponent;
  let fixture: ComponentFixture<CadastrarRemocaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastrarRemocaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastrarRemocaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
