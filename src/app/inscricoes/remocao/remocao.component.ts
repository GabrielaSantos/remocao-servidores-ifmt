import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/modelos/login/login.service';
import { BloqueiosService } from 'src/app/modelos/bloqueios/bloqueios.service';
import { RemocaoService } from 'src/app/modelos/inscricoes/remocao.service';
import * as moment from 'moment'


@Component({
  selector: 'app-remocao',
  templateUrl: './remocao.component.html',
  styleUrls: ['./remocao.component.scss']
})
export class RemocaoComponent implements OnInit {
  cadastro_remocao = true
  bloqueio_remocao = false
  data_bloqueio: any
  motivo_bloqueio: string
  login: any
  dataSourceRemocao: any
  dataSourceBloqueio: any

  constructor(private _apiLogin: LoginService, private _apiInscricao: RemocaoService, private _apiBloqueios: BloqueiosService) { }

  async ngOnInit() {

    try {
      this.login = await this.getLogin()
      this.dataSourceRemocao = await this.getInscricao()
      this.dataSourceBloqueio = await this.getBloqueio()
    }

    catch (err) {
      console.error(err)
    }

  }

  getLogin() {
    return new Promise((resolve, reject) => {
      resolve(this._apiLogin.getLogin())
    })
  }

  getInscricao() {
    return new Promise((resolve, reject) => {
      this._apiInscricao.getUltimaInscricao(this.login.id)
        .subscribe((res: any) => {
          if(res.data.length > 0) {
            this.cadastro_remocao = false
          }
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  getBloqueio() {
    return new Promise((resolve, reject) => {      
      this._apiBloqueios.getUltimoBloqueio(this.login.id)
        .subscribe((res: any) => {          
          if(res.data.length > 0) {
            this.bloqueio_remocao = true
            this.data_bloqueio = this.dataFormat(res.data[0].data_liberacao)
            this.motivo_bloqueio = res.data[0].justificativa
          }
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }
  
  dataFormat(data) {
    return moment(data).utc().format('DD/MM/YYYY')
  }

}
