import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'

import { LoginService } from 'src/app/modelos/login/login.service';
import { RemocaoService } from 'src/app/modelos/inscricoes/remocao.service';
import { ListasService } from 'src/app/modelos/listas/listas.service';
import { DadosService } from 'src/app/modelos/inscricoes/dados.service';
import { CampusService } from 'src/app/modelos/campus/campus.service';
import { Router } from '@angular/router';
import { BloqueiosService } from 'src/app/modelos/bloqueios/bloqueios.service';

@Component({
  selector: 'app-lista-remocao',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.scss']
})
export class ListaComponent implements OnInit {
  login: any
  inscricao: any
  campus: string
  verificarAprovacao: any

  dataSourceDados: any
  dataSourceCampus:any
  dataSourceRemocao: any
  dataSourceLista: any
  inscricaoForm: FormGroup

  constructor(private _apiDados: DadosService, private _apiLogin: LoginService,
     private _apiInscricao: RemocaoService, private _apiLista: ListasService, 
     private _apiBloqueios: BloqueiosService,
     private formBuilder: FormBuilder, private _apiCampus: CampusService, private router: Router) { }

  async ngOnInit() {
    this.criarFormulario()

    try {
      this.login = await this.getLogin()
      this.dataSourceDados = await this.getUsuario()
      this.dataSourceRemocao = await this.getInscricao()
      this.verificarAprovacao = this.dataSourceRemocao[0].aprovacao

      this.dataSourceLista = await this.getLista(this.dataSourceRemocao[0].campus, this.dataSourceDados[0].cargo)
     
      this.dataSourceCampus = await this.getCampus(this.dataSourceRemocao[0].campus)
      this.campus = this.dataSourceCampus[0].nome

      this.setDadosPessoais(this.campus, this.dataSourceLista[0].id)
      
    }

    catch (err) {
      console.error(err)
    }

  }

  getLogin() {
    return new Promise((resolve, reject) => {
      resolve(this._apiLogin.getLogin())
    })
  }

  getInscricao() {
    return new Promise((resolve, reject) => {
      this._apiInscricao.getUltimaInscricao(this.login.id)
        .subscribe((res: any) => {   
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  getCampus(id) {
    return new Promise((resolve, reject) => {
      this._apiCampus.getCampusID(id)
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  getUsuario() {
    return new Promise((resolve, reject) => {
      this._apiDados.getUsuarioID(this.login.id)
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  getLista(campus, cargo) {
    return new Promise((resolve, reject) => {
      this._apiLista.getLista(campus, cargo)
        .subscribe((res: any) => {   
          res = res.filter((element) => {      
            return element.id_user == this.login.id 
          }) 
          resolve(res)
        }, err => {
          reject(err)
        })
    })   
  }

  criarFormulario() {
    this.inscricaoForm = this.formBuilder.group({
      'posicao': [null, Validators.required],
      'campus': [null, Validators.required]
    })
  }

  setDadosPessoais(campus, posicao) {
    this.inscricaoForm.setValue({
      posicao: posicao,
      campus: campus
    })
  }

  setInscricao() {
    return new Promise((resolve, reject) => {
      let newForm = {
        ativar: 0
      }

      this._apiInscricao.patchInscricao(newForm, this.dataSourceRemocao[0].id)
        .subscribe((res: any) => {   
          resolve(res)
        }, err => {
          reject(err)
        })
    })
  }

  setBloqueio(mensagem) {
    let bloqueio = {
      usuario: this.login.id,
      justificativa: mensagem
    }

    return new Promise((resolve, reject) => {
      this._apiBloqueios.postBloqueios(bloqueio)
        .subscribe((res: any) => {   
          resolve(res)
        }, err => {
          reject(err)
        })
    })
  }

  setAprovacao() {
    let aprovacao = {
      aprovacao: 2
    }

    return new Promise((resolve, reject) => {
      this._apiInscricao.patchInscricao(aprovacao, this.dataSourceRemocao[0].id)
        .subscribe((res: any) => {   
          resolve(res)
        }, err => {
          reject(err)
        })
    })
  }


  async cancelar(event) {

    try {
      await this.setInscricao()
      this.setBloqueio("Inscrição para Cadastro de Remoção Permanente Cancelada!")
      window.location.reload();
    }

    catch (err) {
      console.error(err)
    }

  }

  async cancelarRemocao(event) {

    try {
      await this.setInscricao()
      this.setBloqueio("Aprovação de Remoção Rejeitada!")
      window.location.reload();
    }

    catch (err) {
      console.error(err)
    }
  }

  async aceitarRemocao(event) {

    try {
      await this.setAprovacao()
      window.location.reload();
    }

    catch (err) {
      console.error(err)
    }
  }
}
