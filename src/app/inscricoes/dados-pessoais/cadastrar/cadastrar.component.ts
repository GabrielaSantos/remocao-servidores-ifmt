import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as moment from 'moment';

import { MessageService } from 'src/app/message.service';
import { Usuario, DadosService } from 'src/app/modelos/inscricoes/dados.service';
import { Campus, CampusService } from 'src/app/modelos/campus/campus.service';
import { Cargo, CargosService } from 'src/app/modelos/cargos/cargos.service';
import { LoginService } from 'src/app/modelos/login/login.service';

@Component({
  selector: 'app-cadastrar',
  templateUrl: './cadastrar.component.html',
  styleUrls: ['./cadastrar.component.scss']
})

export class CadastrarDadosComponent implements OnInit {
  login: any

  definir_cargo = false
  usuarioForm: FormGroup;
  dataSource: Usuario[] = []
  alerta_erro: boolean = false
  dataSourceCampus: Campus[] = []
  dataSourceCargo: Cargo[] = []

  get matricula() { return this.usuarioForm.get('matricula'); }

  constructor(private router: Router, private _api: DadosService, private formBuilder: FormBuilder,
              private _apiCampus: CampusService, private messageService: MessageService,
              private _apiCargo: CargosService, private _apiLogin: LoginService) {
  }

  ngOnInit() {
    this.login = this.getLogin()
    
    this.criarFormulario()

    this._apiCampus.getCampus()
    .subscribe((res:any) => {
      this.dataSourceCampus = res.data;
    }, err => {
      console.log(err);
    });      
  }

  getLogin() {
    return this._apiLogin.getLogin()
  }

  onChangeArea(deviceValue) {
    this.definir_cargo = true

    this._apiCargo.getCargoArea(deviceValue)
    .subscribe((res:any) => {
      this.dataSourceCargo = res.data
    }, err => {
      console.log(err)
    })
  }

  postUsuario(data: any) {
    let form = {}


    for (let control in data.controls) {
      if(control != 'area' && control != 'recaptchaReactive'){
        form[control] = data.value[control]
      }        
    }

    form['data_ingresso_ifmt'] = this.dataFormat(form['data_ingresso_ifmt'])
    form['data_ingresso_campus'] = this.dataFormat(form['data_ingresso_campus'])
    form['nascimento'] = this.dataFormat(form['nascimento'])

    this._api.postUsuario(form)
      .subscribe(res => {
          this.alerta_erro = false
          this.messageService.sendMessage('Cadastro OK', 'inscricoes')
          this.router.navigate(['/Inscricoes']);
        }, (err) => {
          console.log(err);
          this.alerta_erro = true
        });
  }

  criarFormulario() {
    this.usuarioForm = this.formBuilder.group({
      'nome': [null, Validators.required],
      'matricula': [null, Validators.required],
      'email': [null, Validators.required],
      'telefone': [null, Validators.required],
      'nascimento': [null, Validators.required],
      'campus_atual': [null, Validators.required],
      'data_ingresso_campus': [null, Validators.required],
      'data_ingresso_ifmt': [null, Validators.required],
      'area': [null, Validators.required],
      'cargo': [null, Validators.required],
      'recaptchaReactive': [null, Validators.required]
    })    
  }

  dataFormat(data) {
    return moment(data).utc().format('YYYY-MM-DD')
  }

  resolved(captchaResponse: string) {
    console.log(`Resolved captcha with response: ${captchaResponse}`);
  }

}
