import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment'

import { MensagensService } from 'src/app/modelos/mensagens/mensagens.service';
import { LoginService } from 'src/app/modelos/login/login.service';


@Component({
  selector: 'app-avaliacoes',
  templateUrl: './avaliacoes.component.html',
  styleUrls: ['./avaliacoes.component.scss']
})
export class AvaliacoesComponent implements OnInit {
  dados: any
  login: any
  dataSourceMensagens: any

  constructor(private router: Router, private _apiMensagens: MensagensService, private _apiLogin: LoginService) {
  }

  async ngOnInit() {

    try {
      this.login = await this.getLogin()
      this.dataSourceMensagens = await this.getMensagens(this.login.id)
    }

    catch (err) {
      console.error(err)
    }
  }

  getLogin() {
    return new Promise((resolve, reject) => {
      resolve(this._apiLogin.getLogin())
    })
  }

  getMensagens(id_user) {
    return new Promise((resolve, reject) => {
      this._apiMensagens.getMensagem(id_user)
        .subscribe((res: any) => {
          
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  dataFormat(data) {
    return moment(data).utc().format('DD/MM/YYYY')
  }
}
