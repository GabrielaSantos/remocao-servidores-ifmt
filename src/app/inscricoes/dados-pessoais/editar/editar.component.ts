import { Component, OnInit, AfterViewInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import * as moment from 'moment'

import { CampusService } from 'src/app/modelos/campus/campus.service'
import { DadosService } from 'src/app/modelos/inscricoes/dados.service'
import { CargosService } from 'src/app/modelos/cargos/cargos.service'
import { MessageService } from 'src/app/message.service'
import { LoginService } from 'src/app/modelos/login/login.service'

let self: EditarDadosComponent

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.scss']
})
export class EditarDadosComponent implements OnInit {
  
  dados: any
  login: any
  usuarioForm: FormGroup

  area: any
  dataSource: any
  dataSourceCampus:any
  dataSourceCargoArea: any

  get matricula() { return this.usuarioForm.get('matricula') }

  constructor(private router: Router, private messageService: MessageService, private _api: DadosService,
    private formBuilder: FormBuilder, private _apiCampus: CampusService, private _apiCargo: CargosService, private _apiLogin: LoginService) {
    self = this
  } 

  async ngOnInit() {
    this.criarFormulario()

    try {
      this.login = await this.getLogin()
      this.dataSource = await this.getUsuario()

      this.dataSourceCampus = await this.getCampus()
      self.area = await this.getArea(this.dataSource[0].cargo)

      this.dataSourceCargoArea = await this.getCargoArea(self.area)
      this.setDadosPessoais(this.dataSource[0])
    }

    catch (err) {
      console.error(err)
    }
  }

  onChangeArea(deviceValue) {
    this._apiCargo.getCargoArea(deviceValue)
      .subscribe((res: any) => {
        this.dataSourceCargoArea = res.data
      }, err => {
        console.log(err)
      })
  }

  getLogin() {
    return new Promise((resolve, reject) => {
      resolve(this._apiLogin.getLogin())
    })
  }

  getUsuario() {
    return new Promise((resolve, reject) => {
      this._api.getUsuarioID(this.login.id)
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  getCampus() {
    return new Promise((resolve, reject) => {
      this._apiCampus.getCampus()
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  getArea(cargoAtual) {
    return new Promise((resolve, reject) => {
      this._apiCargo.getCargoID(cargoAtual)
        .subscribe((res: any) => {
          resolve(res.data[0].area)
        }, err => {
          reject(err)
        })
    })
  }

  getCargoArea(area) {
    return new Promise((resolve, reject) => {
      this._apiCargo.getCargoArea(area)
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  patchUsuario(form: any) {
    let newForm = {}

    for (let control in form.controls) {
      if (form.controls[control].dirty && control != 'area' && control != 'recaptchaReactive') {
          newForm[control] = form.value[control]
      }
    }

    if (newForm['data_ingresso_ifmt'])
      newForm['data_ingresso_ifmt'] = this.dataFormat(newForm['data_ingresso_ifmt'].value)

    if (newForm['data_ingresso_campus'])
      newForm['data_ingresso_campus'] = this.dataFormat(newForm['data_ingresso_campus'].value)

    if (newForm['nascimento'])
      newForm['nascimento'] = this.dataFormat(newForm['nascimento'].value)

    newForm['aprovacao_dados'] = 0

    this._api.patchUsuario(newForm, this.dataSource[0].id)
      .subscribe(res => {
        this.messageService.sendMessage('Edição OK', 'inscricoes')
        this.router.navigate(['/Inscricoes'])
      }, (err) => {
        console.log(err)
      })
  }

  criarFormulario() {
    this.usuarioForm = this.formBuilder.group({
      'nome': [null, Validators.required],
      'matricula': [null, Validators.required],
      'email': [null, Validators.required],
      'telefone': [null, Validators.required],
      'nascimento': [null, Validators.required],
      'campus_atual': [null, Validators.required],
      'data_ingresso_campus': [null, Validators.required],
      'data_ingresso_ifmt': [null, Validators.required],
      'area': [null, Validators.required],
      'cargo': [null, Validators.required],
      'recaptchaReactive': [null, Validators.required]
    })
  }

  setDadosPessoais(dados) {
    this.usuarioForm.setValue({
      nome: dados.nome,
      matricula: dados.matricula,
      email: dados.email,
      telefone: dados.telefone,
      nascimento: {
        day: Number.parseInt(moment(dados.nascimento).utc().format('DD')),
        month: Number.parseInt(moment(dados.nascimento).utc().format('MM')),
        year: Number.parseInt(moment(dados.nascimento).utc().format('YYYY'))
      },
      campus_atual: dados.campus_atual,
      data_ingresso_campus: {
        day: Number.parseInt(moment(dados.data_ingresso_campus).utc().format('DD')),
        month: Number.parseInt(moment(dados.data_ingresso_campus).utc().format('MM')),
        year: Number.parseInt(moment(dados.data_ingresso_campus).utc().format('YYYY'))
      },
      data_ingresso_ifmt: {
        day: Number.parseInt(moment(dados.data_ingresso_ifmt).utc().format('DD')),
        month: Number.parseInt(moment(dados.data_ingresso_ifmt).utc().format('MM')),
        year: Number.parseInt(moment(dados.data_ingresso_ifmt).utc().format('YYYY'))
      },
      area: self.area,
      cargo: dados.cargo,
      recaptchaReactive: null
    })
  }

  dataFormat(data) {
    return moment(data).utc().format('YYYY-MM-DD')
  }

  resolved(captchaResponse: string) {
    console.log(`Resolved captcha with response: ${captchaResponse}`)
  }
}