import { Component, OnInit } from '@angular/core'
import { MessageService } from 'src/app/message.service'
import { CampusService, Campus } from 'src/app/modelos/campus/campus.service'
import { Router } from '@angular/router'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import * as moment from 'moment'

import { DadosService } from 'src/app/modelos/inscricoes/dados.service'
import { CargosService } from 'src/app/modelos/cargos/cargos.service'
import { LoginService } from 'src/app/modelos/login/login.service'
import { RemocaoService } from 'src/app/modelos/inscricoes/remocao.service'
import { BloqueiosService } from 'src/app/modelos/bloqueios/bloqueios.service'

let self: VisualizarComponent

@Component({
  selector: 'app-visualizar',
  templateUrl: './visualizar.component.html',
  styleUrls: ['./visualizar.component.scss']
})

export class VisualizarComponent implements OnInit {
  dados: any
  login: any
  bloquear_edicao = false
  situacao_usuario = 0
  aprovacao_remocao: any
  usuarioForm: FormGroup
  campus: string
  cargo: string
  area: string
  aprovacao_dados: number
  dataSource: any
  dataSourceCampus:any
  dataSourceCargo: any
  dataSourceArea: any
  dataSourceRemocao: any
  dataSourceBloqueio: any

  get matricula() { return this.usuarioForm.get('matricula') }

  constructor(private router: Router, private messageService: MessageService, private _api: DadosService,
    private formBuilder: FormBuilder, private _apiCampus: CampusService, private _apiCargo: CargosService,
    private _apiLogin: LoginService,  private _apiInscricao: RemocaoService, private _apiBloqueios: BloqueiosService ) {
    self = this
  }

   async ngOnInit() {
    this.criarFormulario()

    try {
      this.login = await this.getLogin()

      this.dataSource = await this.getUsuario()
      this.aprovacao_dados = this.dataSource[0].aprovacao_dados

      this.dataSourceCampus = await this.getCampus(this.dataSource[0].campus_atual)
      this.campus = this.dataSourceCampus[0].nome

      this.dataSourceCargo = await this.getCargo(this.dataSource[0].cargo)
      this.cargo = this.dataSourceCargo[0].nome
      this.area = (this.dataSourceCargo[0].area == 1 ? 'Professor do Ensino Básico, Técnico e Tecnológico' : 'Técnico')


      this.dataSourceRemocao = await this.getInscricao()
      
      this.dataSourceBloqueio = await this.getBloqueio()

      this.setDadosPessoais(this.dataSource[0])
    }

    catch (err) {
      console.error(err)
    }
  }

  getLogin() {
    return new Promise((resolve, reject) => {
      resolve(this._apiLogin.getLogin())
    })
  }

   getUsuario() {
    return new Promise((resolve, reject) => {
      this._api.getUsuarioID(this.login.id)
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  getInscricao() {
    return new Promise((resolve, reject) => {
      this._apiInscricao.getUltimaInscricao(this.login.id)
        .subscribe((res: any) => {
          if(res.data.length > 0) {
            this.bloquear_edicao = true
            this.situacao_usuario = 1
            this.aprovacao_remocao = res.data[0].aprovacao
          }
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  getBloqueio() {
    return new Promise((resolve, reject) => {      
      this._apiBloqueios.getUltimoBloqueio(this.login.id)
        .subscribe((res: any) => {          
          if(res.data.length > 0) {
            this.bloquear_edicao = true
            this.situacao_usuario = 2
          }
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }
  

  getCampus(id) {
    return new Promise((resolve, reject) => {
      this._apiCampus.getCampusID(id)
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  getArea(cargoAtual) {
    return new Promise((resolve, reject) => {
      this._apiCargo.getCargoID(cargoAtual)
        .subscribe((res: any) => {
          resolve(res.data[0].area)
        }, err => {
          reject(err)
        })
    })
  }

  getCargo(cargo) {
    return new Promise((resolve, reject) => {
      this._apiCargo.getCargoID(cargo)
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  editarDados(event) {
    this.messageService.sendMessage(this.login, 'editar-dados')
    this.router.navigate(['/Editar-Dados-Pessoais'])
  }

  visualizarAvaliacao(event) {
    this.messageService.sendMessage(this.login, 'visualizar-avaliacao')
    this.router.navigate(['/Avaliacao-Dados-Pessoais'])
  }
  
  criarFormulario() {
    this.usuarioForm = this.formBuilder.group({
      'nome': [null, Validators.required],
      'matricula': [null, Validators.required],
      'email': [null, Validators.required],
      'telefone': [null, Validators.required],
      'nascimento': [null, Validators.required],
      'campus_atual': [null, Validators.required],
      'data_ingresso_campus': [null, Validators.required],
      'data_ingresso_ifmt': [null, Validators.required],
      'area': [null, Validators.required],
      'cargo': [null, Validators.required]
    })
  }

  setDadosPessoais(dados) {
    this.usuarioForm.setValue({
      nome: dados.nome,
      matricula: dados.matricula,
      email: dados.email,
      telefone: dados.telefone,
      nascimento: {
        day: Number.parseInt(moment(dados.nascimento).utc().format('DD')),
        month: Number.parseInt(moment(dados.nascimento).utc().format('MM')),
        year: Number.parseInt(moment(dados.nascimento).utc().format('YYYY'))
      },
      campus_atual: dados.campus_atual,
      data_ingresso_campus: {
        day: Number.parseInt(moment(dados.data_ingresso_campus).utc().format('DD')),
        month: Number.parseInt(moment(dados.data_ingresso_campus).utc().format('MM')),
        year: Number.parseInt(moment(dados.data_ingresso_campus).utc().format('YYYY'))
      },
      data_ingresso_ifmt: {
        day: Number.parseInt(moment(dados.data_ingresso_ifmt).utc().format('DD')),
        month: Number.parseInt(moment(dados.data_ingresso_ifmt).utc().format('MM')),
        year: Number.parseInt(moment(dados.data_ingresso_ifmt).utc().format('YYYY'))
      },
      area: self.area,
      cargo: dados.cargo
    })
  }

  dataFormat(data) {
    return moment(data).utc().format('YYYY-MM-DD')
  }
}