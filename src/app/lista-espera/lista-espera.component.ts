import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { CampusService } from '../modelos/campus/campus.service';
import { CargosService } from '../modelos/cargos/cargos.service';
import { ListasService } from '../modelos/listas/listas.service';

@Component({
  selector: 'app-lista-espera',
  templateUrl: './lista-espera.component.html',
  styleUrls: ['./lista-espera.component.scss']
})
export class ListaEsperaComponent implements OnInit {
  inscricaoForm: FormGroup
  listaSelecionada: boolean
  definir_cargo: boolean;
  dataSourceCargo: any;
  dataSourceCampus: any
  dataSourceLista:any
  alerta_de_inscrito: boolean;
  itemsPerPage = 20
  currentPage: number = 1;
  campus_selecionado: any
  
  constructor( private _apiLista: ListasService, private formBuilder: FormBuilder, private _apiCampus: CampusService, private _apiCargo: CargosService) { }

  async ngOnInit() {
    try {
      this.criarFormulario()
      this.listaSelecionada = false
      this.dataSourceCampus = await this.getCampus()
    }
     
    catch (err) {
      console.error(err)
    }
  }

  getCampus() {
    return new Promise((resolve, reject) => {
      this._apiCampus.getCampus()
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  criarFormulario() {
    this.inscricaoForm = this.formBuilder.group({
      'campus': [null, Validators.required],
      'area': [null, Validators.required],
      'cargo': [null, Validators.required]
    })    
  }

  onChangeArea(deviceValue) {
    this.definir_cargo = true

    this._apiCargo.getCargoArea(deviceValue)
    .subscribe((res:any) => {
      this.dataSourceCargo = res.data
    }, err => {
      console.log(err)
    })
  }
  
  dataFormat(data) {
    return moment(data).utc().format('DD/MM/YYYY')
  }

  async postInscricao(dados) {
    this.listaSelecionada = true
    this.campus_selecionado = await this.getNomeCampus(dados.campus)
    console.log(this.campus_selecionado)
    this.dataSourceLista = await this.getLista(dados.campus, dados.cargo)
  }

  getNomeCampus(id) {
    let array = {}

    array =  this.dataSourceCampus.filter((item) => {
      return (item.id == id); 
    })
    return array[0].nome
  }

  getLista(campus, cargo) {
    return new Promise((resolve, reject) => {
      this._apiLista.getLista(campus, cargo)
        .subscribe((res: any) => {
          this.alerta_de_inscrito = (res.length > 0 ? false : true)    
          resolve(res)
        }, err => {
          reject(err)
        })
    })
  }



}
