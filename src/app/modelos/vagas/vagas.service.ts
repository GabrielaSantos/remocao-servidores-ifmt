import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { config } from '../../api'

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class VagasService {

  postVaga(vagas): Observable<Vaga> {
    console.log("API: "+vagas)
    return this.http.post<Vaga>(config.apiUrl+'/vagas', vagas, httpOptions).pipe();
  }

  getVaga(id: number): Observable<Vaga> {
    const url = `${config.apiUrl+'/vagas'}?id=${id}&ativar=1&$limit=1`;
    return this.http.get<Vaga>(url).pipe(
      catchError(this.handleError<Vaga>(`id=${id}`))
    );
  }

  getVagas(): Observable<Vaga> {
    const url = `${config.apiUrl+'/vagas'}?ativar=1&$sort[data_atualizacao]=-1`;
    return this.http.get<Vaga>(url).pipe(
      catchError(this.handleError<Vaga>(`error vaga`))
    );
  }

  patchVaga (vagas, id): Observable<Vaga> {
    return this.http.patch<Vaga>(`${config.apiUrl+'/vagas'}/${id}`, vagas, httpOptions).pipe(
    );
  }

  constructor(private http: HttpClient) { }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error);

      return of(result as T);
    };
  }
}

export class Vaga {
  id: number
  ativar: boolean;
  campus: number
  cargo: number
  status: string;
  data_criacao: Date;
  data_atualizacao: Date;
}