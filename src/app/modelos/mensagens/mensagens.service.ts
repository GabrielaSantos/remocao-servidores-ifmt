import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { catchError } from 'rxjs/operators'
import { config } from '../../api'

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})

export class MensagensService {
  getMensagens (): Observable<Mensagem[]> {
    return this.http.get<Mensagem[]>(config.apiUrl+'/mensagens?ativar=1')
      .pipe(
        catchError(this.handleError('getMensagems', []))
      )
  }

  getMensagemID(id: number): Observable<Mensagem> {
    const url = `${config.apiUrl+'/mensagens'}?id=${id}&ativar=1`;
    return this.http.get<Mensagem>(url).pipe(
      catchError(this.handleError<Mensagem>(`id=${id}`))
    );
  }

  getMensagem(usuario: number): Observable<Mensagem> {
    const url = `${config.apiUrl+'/mensagens'}?usuario=${usuario}&ativar=1&$sort[data_criacao]=-1`;
    return this.http.get<Mensagem>(url).pipe(
      catchError(this.handleError<Mensagem>(`usuario=${usuario}`))
    );
  }

  postMensagem(mensagem): Observable<Mensagem> {
    return this.http.post<Mensagem>(config.apiUrl+'/mensagens', mensagem, httpOptions).pipe(
    );
  }

  patchMensagem (mensagem, id): Observable<Mensagem> {
    return this.http.patch<Mensagem>(`${config.apiUrl+'/mensagens'}/${id}`, mensagem, httpOptions).pipe(
    );
  }

  constructor(private http: HttpClient) { }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error)
      return of(result as T)
    }
  }
}

export class Mensagem {
  id: number
  ativar: boolean
  tipo: number
  usuario: number
  mensagem: string
  data_criacao: Date
  data_atualizacao: Date
}