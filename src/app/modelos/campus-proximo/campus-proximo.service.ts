import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { config } from '../../api'

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class CampusProximoService {

  getCampusProximo (id): Observable<CampusProximo[]> {
    return this.http.get<CampusProximo[]>(config.apiUrl+`/campus-proximo?id=${id}&ativar=1`)
      .pipe(
        catchError(this.handleError('getcampus', []))
      );
  }

  getCampusProximoID (id: number): Observable<CampusProximo> {
    const url = `${config.apiUrl+'/campus-proximo'}?campus_origem=${id}&ativar=1&$sort[distancia]=1`;
    return this.http.get<CampusProximo>(url).pipe(
      catchError(this.handleError<CampusProximo>(`getProduto id=${id}`))
    );
  }

  postCampusProximo(campus): Observable<CampusProximo> {
    return this.http.post<CampusProximo>(config.apiUrl+'/campus-proximo', campus, httpOptions).pipe();
  }

  patchCampusProximo (campus, id): Observable<CampusProximo> {
    return this.http.patch<CampusProximo>(`${config.apiUrl+'/campus-proximo'}/${id}`, campus, httpOptions).pipe(
    );
  }


  constructor(private http: HttpClient) { }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error);

      return of(result as T);
    };
  }
}

export class CampusProximo {
  id: number
  ativar: boolean;
  campus_origem: number;
  campus_destino: number;
  distancia: number;
  data_criacao: Date;
  data_atualizacao: Date;
}
