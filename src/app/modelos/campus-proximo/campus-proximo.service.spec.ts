import { TestBed } from '@angular/core/testing';

import { CampusProximoService } from './campus-proximo.service';

describe('CampusProximoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CampusProximoService = TestBed.get(CampusProximoService);
    expect(service).toBeTruthy();
  });
});
