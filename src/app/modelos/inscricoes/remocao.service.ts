import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { config } from '../../api'
import { catchError } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
@Injectable({
  providedIn: 'root'
})
export class RemocaoService {

  postInscricao(inscricao): Observable<Remocao> {
    return this.http.post<Remocao>(config.apiUrl+'/inscricao', inscricao, httpOptions).pipe();
  }

  getInscricao(usuario: number): Observable<Remocao> {
    const url = `${config.apiUrl+'/inscricao'}?usuario=${usuario}&ativar=1`;
    return this.http.get<Remocao>(url).pipe(
      catchError(this.handleError<Remocao>(`usuario=${usuario}`))
    );
  }

  getAllInscricao(): Observable<Remocao> {
    const url = `${config.apiUrl+'/inscricao'}`;
    return this.http.get<Remocao>(url).pipe(
      catchError(this.handleError<Remocao>(`inscricoes`))
    );
  }

  getInscricaoID(id: number): Observable<Remocao> {
    const url = `${config.apiUrl+'/inscricao'}?id=${id}&ativar=1`;
    return this.http.get<Remocao>(url).pipe(
      catchError(this.handleError<Remocao>(`id=${id}`))
    );
  }

  getUltimaInscricao(usuario: number): Observable<Remocao> {
    const url = `${config.apiUrl+'/inscricao'}?usuario=${usuario}&ativar=1&$sort[data_atualizacao]=-1&$limit=1`;
    return this.http.get<Remocao>(url).pipe(
      catchError(this.handleError<Remocao>(`usuario=${usuario}`))
    );
  }

  patchInscricao (usuario, id): Observable<Remocao> {
    return this.http.patch<Remocao>(`${config.apiUrl+'/inscricao'}/${id}`, usuario, httpOptions).pipe(
    );
  }

  constructor(private http: HttpClient) { }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error)
      return of(result as T)
    }
  }
}

export class Remocao {
  id: number
  ativar: boolean
  usuario: number
  campus: number
  aprovacao: number
  data_criacao: Date
  data_aprovacao: Date
  data_atualizacao: Date
}