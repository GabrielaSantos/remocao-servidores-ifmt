import { Injectable } from '@angular/core'
import { Observable, of } from 'rxjs'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { catchError } from 'rxjs/operators'
import { config } from '../../api'

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})

export class DadosService {

  getUsuarios (): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(config.apiUrl+'/usuarios?ativar=1')
      .pipe(
        catchError(this.handleError('getusuarios', []))
      )
  }

  getAllUsuarios (): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(config.apiUrl+'/usuarios')
      .pipe(
        catchError(this.handleError('getusuarios', []))
      )
  }

  getUsuarioID(id: number): Observable<Usuario> {
    const url = `${config.apiUrl+'/usuarios'}?id=${id}&ativar=1`;
    return this.http.get<Usuario>(url).pipe(
      catchError(this.handleError<Usuario>(`id=${id}`))
    );
  }

  getUsuario(matricula: number): Observable<Usuario> {
    const url = `${config.apiUrl+'/usuarios'}?matricula=${matricula}&ativar=1`;
    return this.http.get<Usuario>(url).pipe(
      catchError(this.handleError<Usuario>(`matricula=${matricula}`))
    );
  }

  getUsuariosDados(dados): Observable<Usuario> {
    const url = `${config.apiUrl+'/usuarios'}?aprovacao_dados=${dados}&ativar=1&$sort[data_atualizacao]=1`;
    return this.http.get<Usuario>(url).pipe(
      catchError(this.handleError<Usuario>(`get dados`))
    );
  }

  getUsuarioLike(dados): Observable<Usuario> {
    const url = `${config.apiUrl+'/usuarios'}?&ativar=1&$or[0][nome][$like]=${dados}%&$or[1][matricula][$like]=${dados}%`;
    return this.http.get<Usuario>(url).pipe(
      catchError(this.handleError<Usuario>(`get dados`))
    );
  }


  postUsuario (usuario): Observable<Usuario> {
    return this.http.post<Usuario>(config.apiUrl+'/usuarios', usuario, httpOptions).pipe(
    );
  }

  patchUsuario (usuario, id): Observable<Usuario> {
    return this.http.patch<Usuario>(`${config.apiUrl+'/usuarios'}/${id}`, usuario, httpOptions).pipe(
    );
  }

  constructor(private http: HttpClient) { }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error)
      return of(result as T)
    }
  }
}

export class Usuario {
  id: number
  ativar: boolean
  nome: string
  matricula: string
  email: string
  telefone: string
  nascimento: Date
  campus_atual: number
  cargo: number
  data_ingresso_campus: Date
  data_ingresso_ifmt: Date
  aprovacao_dados: boolean
  permissao: string
  data_criacao: Date
  data_atualizacao: Date
}