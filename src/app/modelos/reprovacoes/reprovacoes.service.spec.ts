import { TestBed } from '@angular/core/testing';

import { ReprovacoesService } from './reprovacoes.service';

describe('ReprovacoesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReprovacoesService = TestBed.get(ReprovacoesService);
    expect(service).toBeTruthy();
  });
});
