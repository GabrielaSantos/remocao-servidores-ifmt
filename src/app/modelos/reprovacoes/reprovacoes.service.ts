import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { config } from '../../api'
import { catchError } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ReprovacoesService {

  postReprovacao(reprovacao): Observable<Reprovacao> {
    return this.http.post<Reprovacao>(config.apiUrl+'/reprovacoes', reprovacao, httpOptions).pipe();
  }

  constructor(private http: HttpClient) { }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error)
      return of(result as T)
    }
  }
}

export class Reprovacao {
  id: number
  justificativa: string
  inscricao: number
  vaga: number
  data: Date
}