import { TestBed } from '@angular/core/testing';

import { AprovacoesService } from './aprovacoes.service';

describe('AprovacoesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AprovacoesService = TestBed.get(AprovacoesService);
    expect(service).toBeTruthy();
  });
});
