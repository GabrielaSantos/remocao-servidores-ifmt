import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { config } from '../../api'
import { catchError } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class AprovacoesService {

  getAprovacao(vaga: number): Observable<Aprovacao> {
    const url = `${config.apiUrl+'/aprovacoes'}?ativar=1&vaga=${vaga}&$sort[data_atualizacao]=-1&$limit=1`;
    return this.http.get<Aprovacao>(url).pipe(
      catchError(this.handleError<Aprovacao>(`vaga=${vaga}`))
    );
  }

  getAllAprovacoes(): Observable<Aprovacao> {
    const url = `${config.apiUrl+'/aprovacoes'}?ativar=1&publicar=1&$sort[data_atualizacao]=-1`;
    return this.http.get<Aprovacao>(url).pipe(
      catchError(this.handleError<Aprovacao>(`aprovado`))
    );
  }

  postAprovacao(aprovacao): Observable<Aprovacao> {
    return this.http.post<Aprovacao>(config.apiUrl+'/aprovacoes', aprovacao, httpOptions).pipe();
  }

  patchAprovacao (aprovacao, id): Observable<Aprovacao> {
    return this.http.patch<Aprovacao>(`${config.apiUrl+'/aprovacoes'}/${id}`, aprovacao, httpOptions).pipe(
    );
  }

  constructor(private http: HttpClient) { }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error)
      return of(result as T)
    }
  }
}

export class Aprovacao {
  id: number
  ativar: number
  aceite: number
  publicar: number
  inscricao: number
  vaga: number
  campus_origem: number
  campus_destino: number
  data_aceite: Date
  data_criacao: Date
  data_atualizacao: Date
}