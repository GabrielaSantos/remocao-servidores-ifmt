import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { config } from '../../api'

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})


export class CargosService {
  teste() {
    console.log("TESTE")
  }

  getCargo (): Observable<Cargo[]> {
    return this.http.get<Cargo[]>(config.apiUrl+'/cargo?ativar=1')
      .pipe(
        catchError(this.handleError('getcargo', []))
      );
  }

  getCargoID (id: number): Observable<Cargo> {
    const url = `${config.apiUrl+'/cargo'}?id=${id}&ativar=1`;
    return this.http.get<Cargo>(url).pipe(
      catchError(this.handleError<Cargo>(`getCargo id=${id}`))
    );
  }

  getCargoArea (area: number): Observable<Cargo> {
    const url = `${config.apiUrl+'/cargo'}?area=${area}&ativar=1`;
    return this.http.get<Cargo>(url).pipe(
      catchError(this.handleError<Cargo>(`getCargo area=${area}`))
    );
  }

  constructor(private http: HttpClient) { }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error);

      return of(result as T);
    };
  }
}

export class Cargo {
  id: number
  ativar: boolean;
  area: number
  nome: string;
  data_criacao: Date;
  data_atualizacao: Date;
}