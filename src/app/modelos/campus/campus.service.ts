import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { config } from '../../api'

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})


export class CampusService {

  getCampus (): Observable<Campus[]> {
    return this.http.get<Campus[]>(config.apiUrl+'/campus?ativar=1&$sort[nome]=1')
      .pipe(
        catchError(this.handleError('getcampus', []))
      );
  }

  getCampusID (id: number): Observable<Campus> {
    const url = `${config.apiUrl+'/campus'}?id=${id}&ativar=1`;
    return this.http.get<Campus>(url).pipe(
      catchError(this.handleError<Campus>(`getProduto id=${id}`))
    );
  }

  postCampus(campus): Observable<Campus> {
    return this.http.post<Campus>(config.apiUrl+'/campus', campus, httpOptions).pipe();
  }

  patchCampus (campus, id): Observable<Campus> {
    return this.http.patch<Campus>(`${config.apiUrl+'/campus'}/${id}`, campus, httpOptions).pipe(
    );
  }


  constructor(private http: HttpClient) { }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error);

      return of(result as T);
    };
  }
}

export class Campus {
  id: number
  ativar: boolean;
  nome: string;
  data_criacao: Date;
  data_atualizacao: Date;
}