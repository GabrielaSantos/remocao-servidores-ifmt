import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError} from 'rxjs/operators';
import { config } from '../../api'

// const httpOptions = {
//   headers: new HttpHeaders({'Content-Type': 'application/json'})
// };

@Injectable({
  providedIn: 'root'
})

export class ListasService {
  getLista(idCampus: number, idCargo:number): Observable<Listas> {
    const url = `${config.apiUrl}/lista?idCampus=${idCampus}&idCargo=${idCargo}`;
    return this.http.get<Listas>(url).pipe(
      catchError(this.handleError<Listas>(`getProduto ${idCampus},${idCargo}`))
    );
  }

  getListaID(id: number): Observable<Listas> {
    const url = `${config.apiUrl+'/lista'}?id_user=${id}`;
    return this.http.get<Listas>(url).pipe(
      catchError(this.handleError<Listas>(`getProduto id=${id}`))
    );
  }

  constructor(private http: HttpClient) { }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error);

      return of(result as T);
    };
  }
}

export class Listas {
  id: number
  campus: number
  cargo: number
  nome: string
  id_user: number
  data_ingresso_campus: Date;
  data_ingresso_ifmt: Date;
  nascimento: Date;
}