import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { config } from '../../api'
import { catchError } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class BloqueiosService {
  postBloqueios(bloqueios): Observable<Bloqueio> {
    return this.http.post<Bloqueio>(config.apiUrl+'/bloqueios', bloqueios, httpOptions).pipe();
  }

  getBloqueioID(id: number): Observable<Bloqueio> {
    const url = `${config.apiUrl+'/bloqueios'}?id=${id}&$limit=1`;
    return this.http.get<Bloqueio>(url).pipe(
      catchError(this.handleError<Bloqueio>(`bloqueios`))
    );
  }

  getUltimoBloqueio(usuario: number): Observable<Bloqueio> {
    const url = `${config.apiUrl+'/bloqueios'}?usuario=${usuario}&ativar=1&$sort[data_atualizacao]=-1&$limit=1`;
    return this.http.get<Bloqueio>(url).pipe(
      catchError(this.handleError<Bloqueio>(`usuario=${usuario}`))
    );
  }

  getBloqueios(): Observable<Bloqueio> {
    const url = `${config.apiUrl+'/bloqueios'}?ativar=1&$sort[data_atualizacao]=-1`;
    return this.http.get<Bloqueio>(url).pipe(
      catchError(this.handleError<Bloqueio>(`bloqueios`))
    );
  }

  patchBloqueio (bloqueio, id): Observable<Bloqueio> {
    return this.http.patch<Bloqueio>(`${config.apiUrl+'/bloqueios'}/${id}`, bloqueio, httpOptions).pipe(
    );
  }


  constructor(private http: HttpClient) { }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error)
      return of(result as T)
    }
  }
}

export class Bloqueio {
  id: number
  ativar: boolean
  usuario: number
  justificativa: string
  data_liberacao: Date
  data_criacao: Date
  data_atualizacao: Date
}