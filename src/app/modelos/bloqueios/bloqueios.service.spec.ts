import { TestBed } from '@angular/core/testing';

import { BloqueiosService } from './bloqueios.service';

describe('BloqueiosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BloqueiosService = TestBed.get(BloqueiosService);
    expect(service).toBeTruthy();
  });
});
