import * as moment from 'moment';
import { Validators } from '@angular/forms';

export function setDadosPessoais(dados, form) {
    console.log("AQUI")
    form.setValue({
      nome: dados.nome,
      matricula: dados.matricula,
      email: dados.email,
      telefone: dados.telefone,
      nascimento: {
        day: Number.parseInt(moment(dados.nascimento).utc().format('DD')),
        month: Number.parseInt(moment(dados.nascimento).utc().format('MM')),
        year: Number.parseInt(moment(dados.nascimento).utc().format('YYYY'))
      },
      campus_atual: dados.campus_atual,
      data_ingresso_campus: {
        day: Number.parseInt(moment(dados.data_ingresso_campus).utc().format('DD')),
        month: Number.parseInt(moment(dados.data_ingresso_campus).utc().format('MM')),
        year: Number.parseInt(moment(dados.data_ingresso_campus).utc().format('YYYY'))
      },
      data_ingresso_ifmt: {
        day: Number.parseInt(moment(dados.data_ingresso_ifmt).utc().format('DD')),
        month: Number.parseInt(moment(dados.data_ingresso_ifmt).utc().format('MM')),
        year: Number.parseInt(moment(dados.data_ingresso_ifmt).utc().format('YYYY'))
      }
    })
    return dados
}

export function  criarFormulario(formBuilder, form) {
    form = formBuilder.group({
      'nome': [null, Validators.required],
      'matricula': [null, Validators.required],
      'email': [null, Validators.required],
      'telefone': [null, Validators.required],
      'nascimento': [null, Validators.required],
      'campus_atual': [null, Validators.required],
      'data_ingresso_campus': [null, Validators.required],
      'data_ingresso_ifmt': [null, Validators.required]
    })

    return form
  }