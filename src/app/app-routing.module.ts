import { CampusProximoEditarComponent } from './administrador/campus/campus-proximo/campus-proximo-editar/campus-proximo-editar.component';
import { CampusProximoCadastrarComponent } from './administrador/campus/campus-proximo/campus-proximo-cadastrar/campus-proximo-cadastrar.component';
import { CampusComponent } from './administrador/campus/campus.component';
import { CargoComponent } from './administrador/cargo/cargo.component';
import { RelatorioComponent } from './relatorio/relatorio.component';
import { CadastroBloqueioComponent } from './administrador/bloqueios/cadastro-bloqueio/cadastro-bloqueio.component';
import { BloqueiosComponent } from './administrador/bloqueios/bloqueios.component';
import { VisualizarVagasComponent } from './administrador/vagas/visualizar-vagas/visualizar-vagas.component';
import { EditarVagasComponent } from './administrador/vagas/editar-vagas/editar-vagas.component';
import { CadastrarVagasComponent } from './administrador/vagas/cadastrar-vagas/cadastrar-vagas.component';
import { VagasComponent } from './administrador/vagas/vagas.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdministradorComponent } from './administrador/administrador.component';
import { InscricoesComponent } from './inscricoes/inscricoes.component';
import { InicioComponent } from './inicio/inicio.component';
import { EditarDadosComponent } from './inscricoes/dados-pessoais/editar/editar.component';
import { CadastrarDadosComponent } from './inscricoes/dados-pessoais/cadastrar/cadastrar.component';
import { AvaliarComponent } from './administrador/aprovacao-dados/avaliar/avaliar.component';
import { AvaliacoesComponent } from './inscricoes/dados-pessoais/avaliacoes/avaliacoes.component';
import { CadastrarRemocaoComponent } from './inscricoes/remocao/cadastrar-remocao/cadastrar-remocao.component';
import { AprovacaoDadosComponent } from './administrador/aprovacao-dados/aprovacao-dados.component';
import { EditarBloqueioComponent } from './administrador/bloqueios/editar-bloqueio/editar-bloqueio.component';
import { ListaEsperaComponent } from './lista-espera/lista-espera.component';
import { CadastrarCampusComponent } from './administrador/campus/cadastrar-campus/cadastrar-campus.component';
import { EditarCampusComponent } from './administrador/campus/editar-campus/editar-campus.component';
import { CadastrarCargoComponent } from './administrador/cargo/cadastrar-cargo/cadastrar-cargo.component';
import { EditarCargoComponent } from './administrador/cargo/editar-cargo/editar-cargo.component';
import { CampusProximoComponent } from './administrador/campus/campus-proximo/campus-proximo.component';

const routes: Routes = [
  { path: ' ', redirectTo: 'Inicio'},
  { path: 'Inicio', component: InicioComponent },
  { path: 'Admin', component: AdministradorComponent},
  { path: 'Admin/AvaliarDados/:id', component: AvaliarComponent },
  { path: 'Admin/Usuarios-Cadastrados', component: AdministradorComponent},
  { path: 'Admin/Aprovacao-Dados', component: AprovacaoDadosComponent},
  { path: 'Admin/Vagas', component: VagasComponent},
  { path: 'Admin/Vagas/Cadastrar', component: CadastrarVagasComponent},
  { path: 'Admin/Vagas/Editar/:id', component: EditarVagasComponent},
  { path: 'Admin/Vagas/Visualizar/:id', component: VisualizarVagasComponent},
  { path: 'Admin/Bloqueios', component: BloqueiosComponent},
  { path: 'Admin/Bloqueios/Cadastrar', component: CadastroBloqueioComponent},
  { path: 'Admin/Bloqueios/Editar/:id', component: EditarBloqueioComponent},
  { path: 'Admin/Campus', component: CampusComponent},
  { path: 'Admin/Campus/Cadastrar', component: CadastrarCampusComponent},
  { path: 'Admin/Campus/Editar/:id', component: EditarCampusComponent},
  { path: 'Admin/Campus-Proximo/:id', component: CampusProximoComponent},
  { path: 'Admin/Campus-Proximo/Cadastrar/:id', component: CampusProximoCadastrarComponent},
  { path: 'Admin/Campus-Proximo/Editar/:id', component: CampusProximoEditarComponent},
  { path: 'Admin/Cargos', component: CargoComponent},
  { path: 'Admin/Cargos/Cadastrar', component: CadastrarCargoComponent},
  { path: 'Admin/Cargos/Editar/:id', component: EditarCargoComponent},
  { path: 'Inscricoes', component: InscricoesComponent },
  { path: 'Inscricao-Remocao', component: CadastrarRemocaoComponent },
  { path: 'Lista-de-Espera', component: ListaEsperaComponent },
  { path: 'Relatorio-Remocao', component: RelatorioComponent },
  { path: 'Dados-Pessoais', component: CadastrarDadosComponent },
  { path: 'Editar-Dados-Pessoais', component: EditarDadosComponent },
  { path: 'Avaliacao-Dados-Pessoais', component: AvaliacoesComponent }  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
