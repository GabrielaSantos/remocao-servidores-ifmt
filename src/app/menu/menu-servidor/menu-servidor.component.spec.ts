import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuServidorComponent } from './menu-servidor.component';

describe('MenuServidorComponent', () => {
  let component: MenuServidorComponent;
  let fixture: ComponentFixture<MenuServidorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuServidorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuServidorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
