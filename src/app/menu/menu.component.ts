import { Component, OnInit } from '@angular/core';
import { LoginService } from '../modelos/login/login.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  permissao: any
  login: any

  constructor(private _apiLogin: LoginService) { }

  async ngOnInit() {

    try {
      this.login = await this.getLogin()
      this.permissao = this.login.permissao
      console.log(this.permissao)
    }

    catch (err) {
      console.error(err)
    }

  }

  getLogin() {
    return new Promise((resolve, reject) => {
      resolve(this._apiLogin.getLogin())
    })
  }

}
