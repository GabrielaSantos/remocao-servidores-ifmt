import { TestBed } from '@angular/core/testing';

import { EnvioEmailService } from './envio-email.service';

describe('EnvioEmailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EnvioEmailService = TestBed.get(EnvioEmailService);
    expect(service).toBeTruthy();
  });
});
