import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

import { CampusService } from '../modelos/campus/campus.service';
import { DadosService } from '../modelos/inscricoes/dados.service';
import { RemocaoService } from '../modelos/inscricoes/remocao.service';
import { AprovacoesService } from '../modelos/aprovacoes/aprovacoes.service';

@Component({
  selector: 'app-relatorio',
  templateUrl: './relatorio.component.html',
  styleUrls: ['./relatorio.component.scss']
})
export class RelatorioComponent implements OnInit {
  alerta_de_remocao: boolean
  dadosRemocao: any
  dataSourceAprovacao: any
  dataSourceUsuarios: any
  dataSourceInscricao: any
  dataSourceUsuario: any
  dataSourceCampus:any
  itemsPerPage = 20
  currentPage: number = 1;

  constructor(private _apiCampus: CampusService, private _apiUsuario: DadosService,
    private _apiInscricao: RemocaoService, private _apiAprovacao: AprovacoesService) { }

  async ngOnInit() {
    try {
      this.dataSourceAprovacao = await this.getAprovacao()
      this.dataSourceCampus = await this.getCampus()
      this.dataSourceInscricao = await this.getInscricao()
      this.dataSourceUsuario = await this.getUsuario()
      this.dadosRemocao = await this.setDados(this.dataSourceAprovacao)  
    }
    catch (err) {
      console.error(err)
    }
  }

  setDados(dados) {
    let array = dados
    this.dataSourceAprovacao.forEach((element, i) => {
     array[i].nome = this.setUser(element.inscricao)
     array[i].campus_origem = this.setCampus(element.campus_origem)
     array[i].campus_destino = this.setCampus(element.campus_destino)
    })
    return array
  }

  setUser(id) {
    let arrayInscricao = {}
    let arrayUsuario = {}

    arrayInscricao =  this.dataSourceInscricao.filter((item) => {
      return (item.id == id); 
    })

    arrayUsuario =  this.dataSourceUsuario.filter((item) => {
      return (item.id == arrayInscricao[0].usuario); 
    })

    return arrayUsuario[0].nome
  }

  setCampus(campus) {
    let array = {}
    array =  this.dataSourceCampus.filter((item) => {
      return (item.id == campus); 
    })
    return array[0].nome
  }


  getAprovacao() {
    return new Promise((resolve, reject) => {
      this._apiAprovacao.getAllAprovacoes()
        .subscribe((res: any) => {         
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  getCampus() {
    return new Promise((resolve, reject) => {
      this._apiCampus.getCampus()
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  getUsuario() {
    return new Promise((resolve, reject) => {
      this._apiUsuario.getAllUsuarios()
        .subscribe((res: any) => {
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  getInscricao() {
    return new Promise((resolve, reject) => {
      this._apiInscricao.getAllInscricao()
        .subscribe((res: any) => { 
          resolve(res.data)
        }, err => {
          reject(err)
        })
    })
  }

  dataFormat(data) {
    return moment(data).utc().format('DD/MM/YYYY')
  }


}
