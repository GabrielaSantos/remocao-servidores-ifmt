import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injectable } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, NgbDatepickerI18n, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { RECAPTCHA_SETTINGS, RecaptchaModule, RecaptchaFormsModule, RecaptchaSettings  } from 'ng-recaptcha';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {NgxPaginationModule} from 'ngx-pagination';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { AdministradorComponent } from './administrador/administrador.component';
import { InscricoesComponent } from './inscricoes/inscricoes.component';
import { InicioComponent } from './inicio/inicio.component';
import { I18n, CustomDatepickerI18n } from './modelos/formatacoes/i18n/datepicker-i18n';
import { NgbDatePTParserFormatter } from './modelos/formatacoes/i18n/dateFormat';
import { MessageService } from './message.service';
import { VisualizarComponent } from './inscricoes/dados-pessoais/visualizar/visualizar.component';
import { EditarDadosComponent } from './inscricoes/dados-pessoais/editar/editar.component';
import { CadastrarDadosComponent } from './inscricoes/dados-pessoais/cadastrar/cadastrar.component';
import { AprovacaoDadosComponent } from './administrador/aprovacao-dados/aprovacao-dados.component';
import { AvaliarComponent } from './administrador/aprovacao-dados/avaliar/avaliar.component';
import { AvaliacoesComponent } from './inscricoes/dados-pessoais/avaliacoes/avaliacoes.component';
import { ListaComponent } from './inscricoes/remocao/lista/lista.component';
import { RemocaoComponent } from './inscricoes/remocao/remocao.component';
import { CadastrarRemocaoComponent } from './inscricoes/remocao/cadastrar-remocao/cadastrar-remocao.component';
import { MenuServidorComponent } from './menu/menu-servidor/menu-servidor.component';
import { MenuAdministradorComponent } from './menu/menu-administrador/menu-administrador.component';
import { MenuAdminComponent } from './menu/menu-admin/menu-admin.component';
import { VagasComponent } from './administrador/vagas/vagas.component';
import { CadastrarVagasComponent } from './administrador/vagas/cadastrar-vagas/cadastrar-vagas.component';
import { EditarVagasComponent } from './administrador/vagas/editar-vagas/editar-vagas.component';
import { VisualizarVagasComponent } from './administrador/vagas/visualizar-vagas/visualizar-vagas.component';
import { BloqueiosComponent } from './administrador/bloqueios/bloqueios.component';
import { CadastroBloqueioComponent } from './administrador/bloqueios/cadastro-bloqueio/cadastro-bloqueio.component';
import { EditarBloqueioComponent } from './administrador/bloqueios/editar-bloqueio/editar-bloqueio.component';
import { RelatorioComponent } from './relatorio/relatorio.component';
import { ListaEsperaComponent } from './lista-espera/lista-espera.component';
import { CampusComponent } from './administrador/campus/campus.component';
import { CadastrarCampusComponent } from './administrador/campus/cadastrar-campus/cadastrar-campus.component';
import { EditarCampusComponent } from './administrador/campus/editar-campus/editar-campus.component';
import { CargoComponent } from './administrador/cargo/cargo.component';
import { CadastrarCargoComponent } from './administrador/cargo/cadastrar-cargo/cadastrar-cargo.component';
import { EditarCargoComponent } from './administrador/cargo/editar-cargo/editar-cargo.component';
import { CampusProximoComponent } from './administrador/campus/campus-proximo/campus-proximo.component';
import { CampusProximoCadastrarComponent } from './administrador/campus/campus-proximo/campus-proximo-cadastrar/campus-proximo-cadastrar.component';
import { CampusProximoEditarComponent } from './administrador/campus/campus-proximo/campus-proximo-editar/campus-proximo-editar.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    AdministradorComponent,
    InscricoesComponent,
    InicioComponent,
    VisualizarComponent,
    EditarDadosComponent,
    CadastrarDadosComponent,
    AprovacaoDadosComponent,
    AvaliarComponent,
    AvaliacoesComponent,
    ListaComponent,
    RemocaoComponent,
    CadastrarRemocaoComponent,
    MenuServidorComponent,
    MenuAdministradorComponent,
    MenuAdminComponent,
    VagasComponent,
    CadastrarVagasComponent,
    EditarVagasComponent,
    VisualizarVagasComponent,
    BloqueiosComponent,
    CadastroBloqueioComponent,
    EditarBloqueioComponent,
    RelatorioComponent,
    ListaEsperaComponent,
    CampusComponent,
    CadastrarCampusComponent,
    EditarCampusComponent,
    CargoComponent,
    CadastrarCargoComponent,
    EditarCargoComponent,
    CampusProximoComponent,
    CampusProximoCadastrarComponent,
    CampusProximoEditarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    RecaptchaModule,
    RecaptchaFormsModule,
    Ng2SearchPipeModule,
    NgxPaginationModule
    ],
  providers: [MessageService,
    I18n, { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n },
    { provide: NgbDateParserFormatter, useClass: NgbDatePTParserFormatter },
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: { siteKey: '6LerK7wUAAAAAKwGMo7urwUpzdwVbr_aNpBGbM4c' } as RecaptchaSettings,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
